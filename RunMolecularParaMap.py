#import ParaMapFunctions as pm
import ParaMapFunctionsParallel as pm
import sys, os, glob
import nibabel as nib
from datetime import datetime
import numpy as np
#from matplotlib import pyplot as plt
#import xlrd
#import sys, os
#import SimpleITK as sitk

if __name__ == "__main__":
	## CODE ACTIVATOR
	print('Started:');print(sys.argv[1]);print(str(datetime.now()));
	path = os.path.normpath(sys.argv[1]);
	splitpath = path.split(os.sep);
	maskflag = 'no' # flag to indicate auto-masking yes/no
	type = 'Molecular'
	fit = 'Differential'
	format = '3D'
	name = splitpath[-2];
	day = splitpath[-1];day = day[0:8];
	if type == 'Molecular':
		parameters = ['dTE','AIP']

	## Read data	
	newres = np.array([0.3, 0.3, 0.3]);
	imarray, orgres, time, imarray_org = pm.prep_img(sys.argv[1],type,format, maskflag, newres);
	print('Done 3D to 4D:');print(str(datetime.now()));

	# ## Plotting function
	# fig = plt.figure()
	# ax = fig.add_subplot(111)
	# tracker = pm.IndexTracker(ax, imarray[0,30,0,:,:,:])
	# fig.canvas.mpl_connect('scroll_event', tracker.onscroll)
	# plt.show()

	## Save the 4D image without masking
	print('Saving 4D w/o mask:');print(str(datetime.now()));
	#imarray_org = imarray_org - np.mean(imarray_org[:,0:4,:,:,:,:],axis=1);imarray_org[imarray_org < 1]=0;
	affine = np.eye(4)
	niiarray = nib.Nifti1Image(imarray_org[0,:,0,:,:,:].astype('uint8'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [4., newres[0], newres[1], newres[2], 1., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'Full4D.nii')); 

	## Save the 4D image with mask
	print('Saving 4D w/ mask:');print(str(datetime.now()));
	#imarray = imarray - np.mean(imarray[:,0:4,:,:,:,:],axis=1);imarray[imarray < 1]=0;
	affine = np.eye(4)
	niiarray = nib.Nifti1Image(imarray[0,:,0,:,:,:].astype('uint8'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [4., newres[0], newres[1], newres[2], 1., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'Full4DMasked.nii')); 

	# # Create linearized 4D
	imarray[np.isnan(imarray)]= 0; 
	imarray_lin = np.exp(imarray/24.09); # Linearized imarray - used for projections and diff.

	# For Molecular Imaging - Flash ends at -20 from end of time - the flash has already been removed though so to get the dTE, get avg(>-20...) - avg(<-20...)

	## Save 4D MIP image - Final Max
	print('Saving MIP:');print(str(datetime.now()));
	MIP = np.max(imarray_lin[:,-30:-20,:,:,:,:],axis=1); MIP = MIP[np.newaxis,:,:,:,:,:];
	affine = np.eye(4);
	niiarray = nib.Nifti1Image(MIP[0,0,0,:,:,:].astype('float64'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [4., newres[0], newres[1], newres[2], 1., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'MIP.nii')); 

	## Save 4D FullMIP image - Final FullMax
	print('Saving FullMIP:');print(str(datetime.now()));
	FullMIP = np.max(imarray_lin[:,0:-20,:,:,:,:],axis=1); FullMIP = FullMIP[np.newaxis,:,:,:,:,:];
	affine = np.eye(4);
	niiarray = nib.Nifti1Image(FullMIP[0,0,0,:,:,:].astype('float64'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [4., newres[0], newres[1], newres[2], 1., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'FullMIP.nii')); 

	## Save 4D AVG image - Final Avg
	print('Saving AIP:');print(str(datetime.now()));
	AIP = np.mean(imarray_lin[:,-30:-20,:,:,:,:],axis=1); AIP = AIP[np.newaxis,:,:,:,:,:];
	affine = np.eye(4);
	niiarray = nib.Nifti1Image(AIP[0,0,0,:,:,:].astype('float64'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [4., newres[0], newres[1], newres[2], 1., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'AIP.nii')); 

	## Save 4D FullAVG image - Final FullAvg
	print('Saving FullAIP:');print(str(datetime.now()));
	FullAIP = np.mean(imarray_lin[:,0:-20,:,:,:,:],axis=1); FullAIP = FullAIP[np.newaxis,:,:,:,:,:];
	affine = np.eye(4);
	niiarray = nib.Nifti1Image(FullAIP[0,0,0,:,:,:].astype('float64'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [4., newres[0], newres[1], newres[2], 1., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'FullAIP.nii')); 

	## Save 4D Base image - Final Baseline
	print('Saving Baseline:');print(str(datetime.now()));
	Baseline = np.mean(imarray_lin[:,-10:,:,:,:,:],axis=1); Baseline = Baseline[np.newaxis,:,:,:,:,:];
	affine = np.eye(4);
	niiarray = nib.Nifti1Image(Baseline[0,0,0,:,:,:].astype('float64'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [4., newres[0], newres[1], newres[2], 1., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'Baseline.nii')); 

	## Save 4D dTE image - Final dTE
	print('Saving dTE:');print(str(datetime.now()));
	dTE = AIP - Baseline; #dTE[dTE < 1]=0;
	affine = np.eye(4);
	niiarray = nib.Nifti1Image(dTE[0,0,0,:,:,:].astype('float64'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [4., newres[0], newres[1], newres[2], 1., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'dTE.nii'));

	## Save 4D Min image - Final Min
	print('Saving LIP:');print(str(datetime.now()));
	LIP = np.min(imarray_lin[:,100:-20,:,:,:,:],axis=1); LIP = LIP[np.newaxis,:,:,:,:,:];
	affine = np.eye(4);
	niiarray = nib.Nifti1Image(LIP[0,0,0,:,:,:].astype('float64'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [4., newres[0], newres[1], newres[2], 1., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'LIP.nii')); 

	## Save 4D FullMin image - Final FullMin (Almost full)
	print('Saving FullLIP:');print(str(datetime.now()));
	FullLIP = np.min(imarray_lin[:,35:-20,:,:,:,:],axis=1); FullLIP = FullLIP[np.newaxis,:,:,:,:,:];
	affine = np.eye(4);
	niiarray = nib.Nifti1Image(FullLIP[0,0,0,:,:,:].astype('float64'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [4., newres[0], newres[1], newres[2], 1., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'FullLIP.nii')); 

	## Save 4D Std image - Final Std
	print('Saving STD:');print(str(datetime.now()));
	STD = np.std(imarray_lin[:,-30:-20,:,:,:,:],axis=1); STD = STD[np.newaxis,:,:,:,:,:];
	affine = np.eye(4);
	niiarray = nib.Nifti1Image(STD[0,0,0,:,:,:].astype('float64'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [4., newres[0], newres[1], newres[2], 1., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'STD.nii')); 

	## Save 4D FlashStd image - Final FlashStd
	print('Saving FlashSTD:');print(str(datetime.now()));
	FlashSTD = np.std(imarray_lin[:,-40:,:,:,:,:],axis=1); FlashSTD = FlashSTD[np.newaxis,:,:,:,:,:];
	affine = np.eye(4);
	niiarray = nib.Nifti1Image(FlashSTD[0,0,0,:,:,:].astype('float64'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [4., newres[0], newres[1], newres[2], 1., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'FlashSTD.nii')); 

	## Save 4D FullStd image - Final FullStd
	print('Saving FullSTD:');print(str(datetime.now()));
	FullSTD = np.std(imarray_lin[:,0:,:,:,:,:],axis=1); FullSTD = FullSTD[np.newaxis,:,:,:,:,:];
	affine = np.eye(4);
	niiarray = nib.Nifti1Image(FullSTD[0,0,0,:,:,:].astype('float64'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [4., newres[0], newres[1], newres[2], 1., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'FullSTD.nii'));  