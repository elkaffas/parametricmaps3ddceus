# def read3D(data, type, format,cut=[[0,0,5,5],[0,0,20,4],[0,0,5,5]]):
# 	# cut=[[10,15,5,5],[50,5,20,4],[15,15,5,5]] #Size reduce with user selected caps
# 	# cut=[[-1,-1,5,5],[-1,-1,5,5],[-1,-1,5,5]] #Automatic size reduce
# 	# cut=[[0,0,5,5],[0,0,20,4],[0,0,5,5]] # Keep original size

# 	#Code Does: 
# 	#1 read in a 4D image or a set of 3D images
# 	#2 Find flashes if any
# 	#3 Apply mask
#     # matrix size reduction parameters
# 	#cut=[[10,15,5,5],[50,5,20,4],[15,15,5,5]] # example 
# 	N_lines_z_axis_cut=cut[0][0:2] #10,10	
# 	N_lines_z_axis_cut_limit=cut[0][2:4]#5,5
# 	N_lines_y_axis_cut=cut[1][0:2]#50,5	
# 	N_lines_y_axis_cut_limit=cut[1][2:4]#20,5
# 	N_lines_x_axis_cut=cut[2][0:2]##[15, 15]	
# 	N_lines_x_axis_cut_limit=cut[2][2:4]#5,5
# 	xmldir = data+('/*.xml');
# 	xmlnamedir = sorted(glob.glob(xmldir));
# 	if len(xmlnamedir)>200:
# 		xmlnamedir = xmlnamedir[0:200];
        
# 	#img, res, timeinitial, shapes, dateStr = read_xmlraw_image_func(xmlnamedir[0])
# 	#imarray = np.zeros((1,len(xmlnamedir),1,shapes[2],shapes[1],shapes[0]),dtype='uint8')
# 	imarray = []#np.zeros((len(xmlnamedir),shapes[2],shapes[1],shapes[0]),dtype='uint8')
# 	timeinitial = -1000; ix=-1;
# 	for xmlname in xmlnamedir:
# 		#imarray[0,xmlnamedir.index(xmlname),0,:,:,:], res, timelast, shapes, dateStr = read_xmlraw_image_func(xmlname)
# 		imi, res, timelast, shapes, dateStr = read_xmlraw_image_func(xmlname)
# 		sz=imi.shape
# 		if timeinitial==-1000: 			
# 			timeinitial = timelast
# 			mp=np.mean(imi,0) 
# 			mpb=mp>threshold_otsu(mp) 
# 			mp1=np.sum(mpb,1)
# 			ix = np.where(mp1>0)
# 			mp0=np.sum(mpb,0)
# 			ix0 = np.where(mp0>0)          
# 			mpz=np.mean(imi,1) 
# 			mpzb=mpz>threshold_otsu(mpz)           
# 			mpz1=np.sum(mpzb,1)
# 			iz = np.where(mpz1>0)     
# 			#ix[0][-1]=sz[1];ix[0][0]=0; #for test
# 			#ix0[0][-1]=sz[2];ix0[0][0]=0; #for test          
# 			#iz[0][-1]=sz[0];iz[0][0]=0; #for test   

# 			if N_lines_y_axis_cut[0]!=0: 
# 			   N_lines_y_axis_cut[0]=max(0,max(N_lines_y_axis_cut[0]*(ix[0][0]>0),ix[0][0]-N_lines_y_axis_cut_limit[0])); 
# 			if N_lines_y_axis_cut[1]!=0:          
# 			   N_lines_y_axis_cut[1]=sz[1]-min(sz[1],max((sz[1]-N_lines_y_axis_cut[1])*(ix[0][-1]<sz[1]),ix[0][-1]+N_lines_y_axis_cut_limit[1]))

# 			if N_lines_x_axis_cut[0]!=0:          
# 			   N_lines_x_axis_cut[0]=max(0,max(N_lines_x_axis_cut[0]*(ix0[0][0]>0),ix0[0][0]-N_lines_x_axis_cut_limit[0])) 
# 			if N_lines_x_axis_cut[1]!=0:          
# 			   N_lines_x_axis_cut[1]=sz[2]-min(sz[2],max((sz[2]-N_lines_x_axis_cut[1])*(ix0[0][-1]<sz[2]),ix0[0][-1]+N_lines_x_axis_cut_limit[1])) 

# 			if N_lines_z_axis_cut[0]!=0: 
# 			   N_lines_z_axis_cut[0]=max(0,max(N_lines_z_axis_cut[0]*(iz[0][0]>0),iz[0][0]-N_lines_z_axis_cut_limit[0])) 
# 			if N_lines_z_axis_cut[1]!=0:          
# 			   N_lines_z_axis_cut[1]=sz[0]-min(sz[0],max((sz[0]-N_lines_z_axis_cut[1])*(iz[0][-1]<sz[1]),iz[0][-1]+N_lines_z_axis_cut_limit[1]))
# 			#print(N_lines_z_axis_cut,N_lines_y_axis_cut,N_lines_x_axis_cut)
# 			print('Orginal image volume size',imi.shape)
    
#              # reduce matrix size      
# 		imi=imi[N_lines_z_axis_cut[0]:sz[0]-N_lines_z_axis_cut[1],N_lines_y_axis_cut[0]:sz[1]- N_lines_y_axis_cut[1],N_lines_x_axis_cut[0]:sz[2]-N_lines_x_axis_cut[1]]
# 		imarray.append(imi)
# 	time = timelast - timeinitial; #total time of cine in seconds.
# 	print('Reduced image volume size',imi.shape)
# 	imarray1 = np.zeros((1,len(xmlnamedir),1,imi.shape[0],imi.shape[1],imi.shape[2]),dtype='uint8')
# 	imarray1[0,:,0,:,:,:] = np.asarray(imarray)
# 	imarray=imarray1

# 	return imarray, res, time;