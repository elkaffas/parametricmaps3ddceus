
# read xmlraw image data: Alireza-September 16, 2016

# define read xml/raw image data
def read_xmlraw_image_func( filename):
    
   # imports
   import xml.etree.ElementTree as ET
   import numpy as np
   #import os 
    
    # get .raw filename
   filename_raw=filename[0:len(filename)-3]+'0.raw';

   # parsing xml file
   tree = ET.parse(filename);
   root = tree.getroot();
   for i in range(0, len(root)):
       if  root[i].tag=='Columns':
           M=int(root[i].text);
       if  root[i].tag=='Rows':
          N=int(root[i].text);  
       if  (root[i].find('Geometry') is None) is False:
          P=int(root[i].find('Geometry').find('Layers').find('Layer').find('RegionLocationMaxz1').text)+1;
          voxelX=float(root[i].find('Geometry').find('Layers').find('Layer').find('PhysicalDeltaX').text);
          voxelY=float(root[i].find('Geometry').find('Layers').find('Layer').find('PhysicalDeltaY').text);
          voxelZ=float(root[i].find('Geometry').find('Layers').find('Layer').find('PhysicalDeltaZ').text);
          voxel=[voxelX, voxelY, voxelZ]; 
       if  root[i].tag=='AcquisitionDateTime':
          tval=root[i].text;
          dateStr=tval[0:4]+'-'+tval[4:6]+'-'+tval[6:8]+' '+tval[8:10]+':'+tval[10:12]+':'+tval[12:len(tval)];
          time=float(tval[12:len(tval)])+float(tval[10:12])*60+float(tval[8:10])*3600;

   # print(M,N,P,voxel,tval,dateStr,time) 

   # read image data from .raw
   f = open(filename_raw, "rb");
   x = np.fromfile(f,dtype=np.uint8);
   img=np.reshape(x, (P,N,M)); 
   return [img, voxel, time, dateStr];  



