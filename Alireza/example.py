
# example: Alireza-September 16, 2016

filename='/Users/ahmedelkaffas/Desktop/Code/PythonParametricMaps/Test3DCEUSData/3Ds/00000.xml';  # example

import numpy as np
import matplotlib.pyplot as plt
from read_xmlraw_image import read_xmlraw_image_func
   
img, voxel, time, dateStr=read_xmlraw_image_func( filename )
print(voxel, time, dateStr)
np.shape(img)

slice_index=120

z=np.squeeze(img[slice_index-1,:,:])

imgplot =plt.imshow(z)

imgplot.set_cmap('gray')