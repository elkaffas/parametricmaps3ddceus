

# this code calculates a #D bounding box for an image volume
import numpy as np
from get_bounding_box import get_bounding_box_3d_func


# imageVec is the input image volume with dimensions time,z,y,x
# to get imgVec you need to run example_dir.py
# if it is a list, rn this command: imgVec = np.asarray(img)
#data_path = 'C:/Users/aakhbardeh/Desktop/ST/AhmedData/'
#img, voxels, time, dateStr=read_xmlraw_directory(data_path)
#imgVec = np.asarray(img)
imgVec=np.squeeze(imgVec)

# threshold 1 is for thresholding of the target image along dimension 1
# disk_size 1 is for removing small subjects in the target image along dimension 1
threshold1=100
disk_size1=10
# threshold 2 is for thresholding of the target image along dimension 2
# disk_size 2 is for removing small subjects in the target image along dimension 2
threshold2=100
disk_size2=3

coord3d=get_bounding_box_3d_func(imgVec,threshold1,disk_size1,threshold2,disk_size2)

print(coord3d)

