
# this function removes frames around flashing: Alireza-September 16, 2016

# define remove flash function
def remove_flash_func( imgVec,th,rem_left,rem_right):
    
   # imports
   import numpy as np
   # inputs:
   # imgVec: 1,time,1,z,y,x
   #th: threshold to detect flashing. default=10
   # rem_left=left-side window to remove flashing effected frames. Default=25
   # rem_right=right-side window to remove flashing effected frames. Default=40
   imgVec_diff=np.diff(np.squeeze(imgVec),axis=0)
   v=np.mean(imgVec_diff,-1)
   del imgVec_diff
   v=np.mean(v,-1)
   v=np.mean(v,-1)
   v=v-v[0]
   f=np.concatenate((np.array([0]),v), axis=0)
   t=np.linspace(1, len(v)+1, num=len(v)+1,dtype='uint8')
   t1=t[f>th]
   t2=t[f<-th]
   if len(t1)>0 and len(t2)>0:
     rem_ix=np.linspace(t2[0]-rem_left, t2[len(t2)-1]+rem_right, num=t2[len(t2)-1]+rem_right-t2[0]+rem_left+1,dtype='uint8')
   keep_time=np.setxor1d(t,rem_ix)

   # imgVec without flash effected frames
   imgVec_new=imgVec[:,keep_time-1,:,:,:,:]
   
   return [imgVec_new,t,f,keep_time]