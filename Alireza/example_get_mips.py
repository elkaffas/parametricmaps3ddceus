
# this code example calculates mip images
import numpy as np
from get_mips import get_mip_images_func


# imageVec is the input image volume with dimensions time,z,y,x
# to get imgVec you need to run example_dir.py
# if it is a list, rn this command: imgVec = np.asarray(img)
#data_path = 'C:/Users/aakhbardeh/Desktop/ST/AhmedData/'
#img, voxels, time, dateStr=read_xmlraw_directory(data_path)
#imgVec = np.asarray(img)
imgVec=np.squeeze(imgVec)

start_frame=0
end_frame=len(imgVec)
mip_max1,mip_min1,mip_mean1=get_mip_images_func(imgVec,start_frame,end_frame)