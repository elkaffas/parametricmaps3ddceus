


# this example removes frames around flashing: Alireza-September 16, 2016
 # imports
import numpy as np
import matplotlib.pyplot as plt
import example_dir
from remove_flash_effected_frames import remove_flash_func

# inputs:
# imgVec: 1,time,1,z,y,x
# get image volume data
example_dir
# convert to 1,t,1,z,y,x format
imgVec = np.asarray(img)
sz=np.shape(imgVec)
im=np.zeros((1,sz[0],1,sz[1],sz[2],sz[3]),dtype='uint8')
im[0,:,0,:,:,:]=imgVec
del img,imgVec

#th: threshold to detect flashing. default=10
th=10
# rem_left=left-side window to remove flashing effected frames. Default=25
rem_left=4
# rem_right=right-side window to remove flashing effected frames. Default=40
rem_right=5

new_im,t,f,keep_time=remove_flash_func(im,th,rem_left,rem_right)
plt.plot(t,f,keep_time,f[keep_time-1])
    


