

# get boundingbox in 3d: Alireza-September 16, 2016

# define 


def   get_bounding_box_3d_func( imgVec, threshold1,disk_size1,threshold2,disk_size2):  

      # imports
      #import numpy as np
      
     # imageVec is the input image volume with dimensions time,z,y,x
     # if dimensions are 1,time,1,z,y,x: squeeze is applied
     
     
     # threshold 1 is for thresholding of the target image along dimension 1
     # disk_size 1 is for removing small subjects in the target image along dimension 1

     # threshold 2 is for thresholding of the target image along dimension 2
     # disk_size 2 is for removing small subjects in the target image along dimension 2
     
     dimension1=1  # it uses imgVec(time,0,:,:) to get the first 2D bounding box
     dimension2=3  # it uses imgVec(time,:,:,i) to get the second 2D bounding box
     start_point=0 # start point for the first get_bounding_box_2d_func
     
     import numpy as np
     mip=np.max(imgVec,-1)
     coord1=get_bounding_box_2d_func( mip,threshold1,disk_size1,dimension1,start_point)       
     coord2=get_bounding_box_2d_func( mip,threshold2,disk_size2,dimension2,coord1[0,1])
     coord3=[[coord2[0,0], coord2[0,1], coord1[0,1]],[coord2[1,0], coord2[1,1], coord1[1,1]]]
     return coord3


    
# define read xml/raw image data
def get_bounding_box_2d_func( mip,threshold,disk_size,ax,image_index):
      # imports
      import numpy as np   
      #from skimage import io, filters
      from skimage.morphology import opening, disk
      from skimage.measure import label, regionprops
      
      # get binary image      

      if ax ==1:
           I=np.squeeze(mip[image_index,:,:])
      elif ax==2:
           I=np.squeeze(mip[:,image_index,:]) 
      elif ax==3:
           I=np.squeeze(mip[:,:,image_index])
      else:
          I=np.squeeze(mip[image_index,:,:])          
          
      Ib=I>threshold
      selem = disk(disk_size)  
      opened = opening(Ib, selem)
      label_img = label(opened)
      regions = regionprops(label_img)
      for props in regions:
           minr, minc, maxr, maxc = props.bbox
      coord=np.matrix([[minr, minc],[maxr, maxc]])
      return coord

