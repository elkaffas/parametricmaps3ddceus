# -*- coding: utf-8 -*-
"""
Created on Thu Nov  3 13:43:28 2016

@author: aakhbardeh
"""

def apply_mask_std(imarray,type='conservative',lowcap_std=14):    

    #type='conservative'
    if type is 'conservative':
        #print('conservative mask')
        ball_size=10;
    else:
        ball_size=5;
        
    # squueze data to reduce dimension from 6d to 4d
    imarray=np.squeeze(imarray);
    
    #del imarray;
    
    # look at distribution and detect changes using std
    std=np.std(imarray,0); #std=np.std(np.squeeze(imarray),0);
    
    #get binay image using otsu
    mask_raw=std>np.max([lowcap_std, 0.55*threshold_otsu(std)])
    
    # remove background noise 
    mask_original=scipy.ndimage.median_filter(mask_raw, size=(10,10,10)) 
    mask_original=scipy.ndimage.median_filter(mask_original, size=(5,5,5))
    del mask_raw;
    
    # close holes
    mask_filled=scipy.ndimage.maximum_filter(mask_original, size=(5,5,5))
    mask_filled = closing(mask_filled, ball(ball_size));
    
    # repeat: close holes
    if type is 'conservative':
           mask_filled=scipy.ndimage.maximum_filter(mask_filled, size=(10,10,10))
           mask_filled=scipy.ndimage.median_filter(mask_filled, size=(10,10,10))
           
    # remove remaining background
    x=mask_filled*np.mean(imarray,0);    
    mask_filled=(x>0.5*threshold_otsu(x))
    del x;
    # remove background noise 
    mask_filled=scipy.ndimage.median_filter(mask_filled, size=(10,10,10)) 
    mask_filled=scipy.ndimage.median_filter(mask_filled, size=(5,5,5))
    
    # close holes
    mask_filled=scipy.ndimage.maximum_filter(mask_filled, size=(2,2,2))  
     
    imarray = np.ma.array(imarray, mask=imarray*(mask_filled==0));
    
    imarray =imarray [np.newaxis,:,np.newaxis,:,:,:]
    
    return imarray,mask_filled,mask_original
    
    
    