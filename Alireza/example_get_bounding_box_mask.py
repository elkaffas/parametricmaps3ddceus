
# example code to get 3d mask image
import numpy as np
import time
import matplotlib.pyplot as plt
import example_dir
from get_bounding_box_mask import get_bounding_box_3d_func

tic = time.clock()

# get image volume data
example_dir

# convert to 4d array
imgVec = np.asarray(img)

# get 3d mask image volume
threshold=50
disk_size=10

mask3d,mask3d_freeshape=get_bounding_box_3d_func(imgVec,threshold,disk_size)

# view result maks images


plt.figure(1) 
imgplot =plt.imshow(np.squeeze(mask3d[:,:,20]))
imgplot.set_cmap('gray')

plt.figure(2) 
#imgplot2 =plt.imshow(np.squeeze(mask3d[:,100,:]))
#imgplot2.set_cmap('gray')
imgplot =plt.imshow(np.squeeze(mask3d_freeshape[:,:,20]))
imgplot.set_cmap('gray')


toc = time.clock()
print(toc - tic)

