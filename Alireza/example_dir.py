
data_path = 'C:/Users/aakhbardeh/Desktop/ST/AhmedData/'

import numpy as np
import matplotlib.pyplot as plt
from read_xmlraw_image import read_xmlraw_directory


img, voxels, time, dateStr=read_xmlraw_directory(data_path)

  
np.shape(img)

slice_index=120
time_index=20

z=np.squeeze(img[time_index][slice_index-1,:,:])

imgplot =plt.imshow(z)

imgplot.set_cmap('gray')


