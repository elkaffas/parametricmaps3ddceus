function [img voxel time dateStr]=read_xmlraw_image(filename)

xl=xml2struct(filename);

% extract image size info
M=str2double(xl.TopLayerObj.Columns.Text);
N=str2double(xl.TopLayerObj.Rows.Text);
P=str2double(xl.TopLayerObj.Geometries.Geometry{1,1}.Layers.Layer{1,1}.RegionLocationMaxz1.Text)+1;

voxelX=str2double(xl.TopLayerObj.Geometries.Geometry{1,1}.Layers.Layer{1,1}.PhysicalDeltaX.Text);
voxelY=str2double(xl.TopLayerObj.Geometries.Geometry{1,1}.Layers.Layer{1,1}.PhysicalDeltaY.Text);
voxelZ=str2double(xl.TopLayerObj.Geometries.Geometry{1,1}.Layers.Layer{1,1}.PhysicalDeltaZ.Text);
voxel=[voxelX voxelY voxelZ];

tval=(xl.TopLayerObj.AcquisitionDateTime.Text);
dateStr=[tval(1:4),'-',tval(5:6),'-',tval(7:8),' ',tval(9:10),':',tval(11:12),':',tval(13:end)];
time=str2double(tval(13:end))+str2double(tval(11:12))*60+str2double(tval(9:10))*3600;
% read image data
fn=[filename(1:end-3),'0.raw'];
fid=fopen(fn);
img=reshape(fread(fid),M,N,P);
fclose(fid);

fclose('all');