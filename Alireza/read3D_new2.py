

def read3D(data, newres=0,cut=[[-1,-1,5,5],[-1,-1,5,5],[-1,-1,5,5]]):
      
	#Code Does: 
	#1 read in a 4D image or a set of 3D images
	#2 Find flashes if any
	#3 Apply mask

     # matrix size reduction parameters
	#cut=[[10,15,5,5],[50,5,20,4],[15,15,5,5]] # example  
	 
		print(cut);N_lines_z_axis_cut=cut[0][0:2] #10,10	
		N_lines_z_axis_cut_limit=cut[0][2:4]#5,5
	
 
		N_lines_y_axis_cut=cut[1][0:2]#50,5	
		N_lines_y_axis_cut_limit=cut[1][2:4]#20,5
	 
		N_lines_x_axis_cut=cut[2][0:2]##[15, 15]	
		N_lines_x_axis_cut_limit=cut[2][2:4]#5,5

		xmldir = data+('/*.xml');
		xmlnamedir = sorted(glob.glob(xmldir));
		if len(xmlnamedir)>170:
			xmlnamedir = xmlnamedir[0:170];
            
		#img, res, timeinitial, shapes, dateStr = read_xmlraw_image_func(xmlnamedir[0])
		#imarray = np.zeros((1,len(xmlnamedir),1,shapes[2],shapes[1],shapes[0]),dtype='uint8')
		imarray = []#np.zeros((len(xmlnamedir),shapes[2],shapes[1],shapes[0]),dtype='uint8')
		timeinitial = -1000; ix=-1; 
		imi_mid, res, timelast, shapes, dateStr = read_xmlraw_image_func(xmlnamedir[np.uint16(len(xmlnamedir)/2)])
		imi10, res, timelast, shapes, dateStr = read_xmlraw_image_func(xmlnamedir[10]) 
  
		for xmlname in xmlnamedir:
			#imarray[0,xmlnamedir.index(xmlname),0,:,:,:], res, timelast, shapes, dateStr = read_xmlraw_image_func(xmlname)
			imi, res, timelast, shapes, dateStr = read_xmlraw_image_func(xmlname)
			sz=imi.shape
			if timeinitial==-1000: 			
 			      timeinitial = timelast
 			      mp=np.mean(imi,0)+np.mean(imi10,0)+np.mean(imi_mid,0);
 			      mpb=mp>0.5*threshold_otsu(mp);  
 			      mp1=np.sum(mpb,1)
 			      ix = np.where(mp1>0)
 			      mp0=np.sum(mpb,0)
 			      ix0 = np.where(mp0>0)          
 			      mpz=np.mean(imi,1)+ +np.mean(imi10,1)+np.mean(imi_mid,1);
 			      mpzb=mpz>0.5*threshold_otsu(mpz)           
 			      mpz1=np.sum(mpzb,1)
 			      iz = np.where(mpz1>0)     
 			      #ix[0][-1]=sz[1];ix[0][0]=0; #for test
 			      #ix0[0][-1]=sz[2];ix0[0][0]=0; #for test          
 			      #iz[0][-1]=sz[0];iz[0][0]=0; #for test   
          
 			      if N_lines_y_axis_cut[0]!=0: 
 			           N_lines_y_axis_cut[0]=max(0,max(N_lines_y_axis_cut[0]*(ix[0][0]>0),ix[0][0]-N_lines_y_axis_cut_limit[0])); 
 			      if N_lines_y_axis_cut[1]!=0:          
 			           N_lines_y_axis_cut[1]=sz[1]-min(sz[1],max((sz[1]-N_lines_y_axis_cut[1])*(ix[0][-1]<sz[1]),ix[0][-1]+N_lines_y_axis_cut_limit[1]))
               
 			      if N_lines_x_axis_cut[0]!=0:          
 			           N_lines_x_axis_cut[0]=max(0,max(N_lines_x_axis_cut[0]*(ix0[0][0]>0),ix0[0][0]-N_lines_x_axis_cut_limit[0])) 
 			      if N_lines_x_axis_cut[1]!=0:          
 			           N_lines_x_axis_cut[1]=sz[2]-min(sz[2],max((sz[2]-N_lines_x_axis_cut[1])*(ix0[0][-1]<sz[2]),ix0[0][-1]+N_lines_x_axis_cut_limit[1])) 
               
 			      if N_lines_z_axis_cut[0]!=0: 
 			           N_lines_z_axis_cut[0]=max(0,max(N_lines_z_axis_cut[0]*(iz[0][0]>0),iz[0][0]-N_lines_z_axis_cut_limit[0])) 
 			      if N_lines_z_axis_cut[1]!=0:          
 			           N_lines_z_axis_cut[1]=sz[0]-min(sz[0],max((sz[0]-N_lines_z_axis_cut[1])*(iz[0][-1]<sz[1]),iz[0][-1]+N_lines_z_axis_cut_limit[1]))
               
 			      #print(N_lines_z_axis_cut,N_lines_y_axis_cut,N_lines_x_axis_cut)
          
 			      print('Orginal image volume size',imi.shape)
        
                 # reduce matrix size      
			imi=imi[N_lines_z_axis_cut[0]:sz[0]-N_lines_z_axis_cut[1],N_lines_y_axis_cut[0]:sz[1]- N_lines_y_axis_cut[1],N_lines_x_axis_cut[0]:sz[2]-N_lines_x_axis_cut[1]]
			
                 # resampling
			if newres!=0:			   
			   imi=resampler_4d(imi, 0, res, newres)
			imarray.append(imi)
  
		time = timelast - timeinitial; #total time of cine in seconds.
		if newres!=0:
                  print('voxel size is changed from ', res, 'to voxel size of ', newres)
		print('Reduced image volume size',imi.shape)
		imarray1 = np.zeros((1,len(xmlnamedir),1,imi.shape[0],imi.shape[1],imi.shape[2]),dtype='uint8')
		imarray1[0,:,0,:,:,:] = np.asarray(imarray)
		imarray=imarray1
  
		return imarray, res, time;
  	 
def resampler_4d(imarray, rtype, curres, newres):      
	#imarray = np.squeeze(imarray) 
	curshape = imarray.shape 
	if len(curshape)==3:
		off=1
	else:
		off=0
  
	axial = curshape[2-off]*curres[1];#y
	lateral = curshape[3-off]*curres[0];#x
	width = curshape[1-off]*curres[2];#z
	
	if rtype == 0: # interpolation based
		#return block_reduce(imarray, block_size=(1,1,1,newres/res[2],newres/res[1],newres/res[0]), fund=np.mean)
		if len(curshape)==3: 
		 imarray2 = skimage.transform.resize(imarray, (int(width/newres[2]),int(axial/newres[1]),int(lateral/newres[0])));
		 imarray2 = np.uint8(imarray2*np.max(imarray)/np.max(imarray2));
		else:            
		 imarray2 = np.zeros((curshape[0],int(width/newres[2]),int(axial/newres[1]),int(lateral/newres[0])),dtype='uint8'); 
		 for t in range(curshape[0]): 
		  im = (skimage.transform.resize(imarray[t,:,:,:], (int(width/newres[2]),int(axial/newres[1]),int(lateral/newres[0]))));
		  imarray2[t,:,:,:] = np.uint8(im*np.max(imarray[t,:,:,:])/np.max(im));
			#imarray2[0,t,0,:,:,:] = scipy.misc.imresize(imarray[t,:,:,:], [int(width/newres[2]),int(axial/newres[1]),int(lateral/newres[0])], interp='bilinear');
	else: 
		if len(curshape)==3: 
		 imarray2 = skimage.transform.resize(imarray, (newshape[0],newshape[1],newshape[2]));
		else:            
		 imarray2 = np.zeros((curshape[0],newshape[0],newshape[1],newshape[2]),dtype='uint8'); 
		 for t in range(curshape[0]): 
		  imarray2 = skimage.transform.resize(imarray[t,:,:,:], (newshape[0],newshape[1],newshape[2]));
			#imarray2[0,t,0,:,:,:] = scipy.misc.imresize(imarray[0,t,0,:,:,:], [newshape[0],newshape[1],newshape[2]], interp='bilinear');
			
	return imarray2;  

def read_xmlraw_image_func(filename):    
	filename_raw=filename[0:len(filename)-3]+('0.raw');
	fff = open(filename_raw,'rb')
	# parsing xml file
	tree = ET.parse(filename);
	root = tree.getroot();
	for i in range(0, len(root)):
	   if  root[i].tag=='Columns':
	      M=int(root[i].text);
	   if  root[i].tag=='Rows':
	      N=int(root[i].text);  
	   if  (root[i].find('Geometry') is None) is False:
	      P=int(root[i].find('Geometry').find('Layers').find('Layer').find('RegionLocationMaxz1').text)+1;
	      voxelX=float(root[i].find('Geometry').find('Layers').find('Layer').find('PhysicalDeltaX').text);
	      voxelY=float(root[i].find('Geometry').find('Layers').find('Layer').find('PhysicalDeltaY').text);
	      voxelZ=float(root[i].find('Geometry').find('Layers').find('Layer').find('PhysicalDeltaZ').text);
	      voxel=[voxelX*10, voxelY*10, voxelZ*10]; 
	   if  root[i].tag=='AcquisitionDateTime':
	      tval=root[i].text;
	      dateStr=tval[0:4]+'-'+tval[4:6]+'-'+tval[6:8]+' '+tval[8:10]+':'+tval[10:12]+':'+tval[12:len(tval)];
	      time=float(tval[12:len(tval)])+float(tval[10:12])*60+float(tval[8:10])*3600;
	#print(M,N,P,voxel,tval,dateStr,time);
	shapes = (M,N,P);
	x = np.fromfile(fff,dtype=np.uint8)
	img = np.reshape(x, (P,N,M))

	return img, voxel, time, shapes, dateStr 