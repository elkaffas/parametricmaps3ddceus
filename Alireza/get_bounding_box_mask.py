

# get boundingbox in 3d: Alireza-September 16, 2016

# define 

def get_bounding_box_3d_func(imgVec,threshold,disk_size):
     # imports
    import numpy as np 
    # threshold is for thresholding of the target image 
    # disk_size is for removing small subjects in the target image    
    mip=np.max(imgVec,0)
    I=np.max(mip,0)
    
    # get bounding box in dimension 1
    coord=[]
    sz=np.shape(mip)
    mask3d_freeshape = np.zeros((sz),dtype='uint8')
    mask3d=mask3d_freeshape 
    for i in range(0,len(mip)):
         I=np.squeeze(mip[i,:,:])
         coordi,maski, mask_freeshapei=get_bounding_box_2d_func( I,threshold,disk_size)
         coord.append(coordi)
         mask3d_freeshape[i,:,:]=mask_freeshapei
         mask3d[i,:,:]=maski
    #coord3d = np.asarray(coord)    
    
    # get bounding box in dimension 3
    mask3d = np.zeros((sz),dtype='uint8')
    for i in range(0,sz[2]):
        #coordi,maski2, mask_freeshapei2=get_bounding_box_2d_func2( np.squeeze(mask3d[:,:,i]),0,disk_size)
        coordi,maski2, mask_freeshapei2=get_bounding_box_2d_func( np.squeeze(mask3d_freeshape[:,:,i]),0,disk_size)
        mask3d[:,:,i]=maski2
        mask3d_freeshape[:,:,i]=mask_freeshapei2
        
    return [mask3d,mask3d_freeshape]

# define bounding_box_2d_function
def get_bounding_box_2d_func( I,threshold,disk_size):
      # imports
      import numpy as np   
      #from skimage import io, filters
      from skimage.morphology import opening, disk,closing
      from skimage.measure import label, regionprops
      
      # get binary image      
       
      Ib=I>threshold
      selem = disk(disk_size)  
      mask2d_freeshape = opening(Ib, selem)
      mask2d_freeshape = closing(mask2d_freeshape, selem)
      label_img = label(mask2d_freeshape)
      regions = regionprops(label_img)
      mask2d = np.zeros(np.shape(I),dtype='uint8')
      for props in regions:
            minr, minc, maxr, maxc = props.bbox
      try:
          minr 
          eflag=1
      except NameError:
          eflag=0
          
      if eflag is 1:
         coord=np.matrix([[minr, minc],[maxr, maxc]])
         mask2d[coord[0,0]:coord[1,0],coord[0,1]:coord[1,1]]=1
      else:
         coord=np.matrix([[float('NaN'), float('NaN')],[float('NaN'), float('NaN')]])
  
      return [coord, mask2d, mask2d_freeshape]
      