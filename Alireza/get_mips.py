

# get mip images: Alireza-September 16, 2016

# define 


def   get_mip_images_func(imgVec,start_frame,end_frame):
      import numpy as np
      # imageVec is the input image volume with dimensions time,z,y,x
      img_diff=[]
      for i in range(start_frame,end_frame):
         img_diff.append(np.squeeze(imgVec[i,:,:,:])-np.squeeze(imgVec[i-1,:,:,:]))    
      
      img_diff_Vec=np.asarray(img_diff)
      mip_max=np.max(img_diff_Vec,0)
      mip_min=np.min(img_diff_Vec,0)
      mip_mean=np.mean(img_diff_Vec,0) 
      return[mip_max,mip_min,mip_mean]