
def masking(imarray):
      from skimage.morphology import opening, disk,closing,ball,erosion,remove_small_objects
      from skimage.measure import label, regionprops
      import scipy.ndimage as nd
      from skimage.filters import sobel
	#Generate Mask
	#Input: imarray - 6D where 0,t,0,z,y,x
	#Output: masked imarray - where 0,t,0,z,y,x
      imarray=np.squeeze(imarray)
      dmip = np.mean(imarray[0:4,:,:,:],axis=0)
      mip=np.max(imarray,axis=0)-dmip;
	#mip=np.divide(np.abs(mip),np.max(imarray2.astype('int16'),axis=1))*1000;
	#diffmaxip, diffminip, diffmeanip = diff_projection(imarray2[0,:,0,:,:,:],0,imarray2.shape[1]);
	# mip = gaussian(mip, sigma=0.8)
      mipb = np.exp(mip) > 1e+17;
     #------------------------- choose the bigest Area -------------- 
      for i in range(0,len(mipb)):
         label_img = label((mipb[i,:,:]))
         #regions = regionprops(label_img)
         labelCount = np.bincount(label_img.ravel())
         labelCount[np.argmax(labelCount)]=0
         if len(labelCount)>2:
            biggest_index = np.argmax(labelCount)
         else:
           biggest_index =-1
         mipb[i,:,:]=nd.binary_fill_holes(sobel((label_img==biggest_index)))==0 
      #---------------------------------------------------------------
      #------------- 3D merging
      mipb = closing(mipb, ball(2));
      mipb = erosion(mipb, ball(4));
      #mipb = opening(mipb, ball(8));
     # mipb = erosion(mipb, ball(14));
	#mipb = erosion(mipb, ball(8));
      
      #------------- save mip/mask
      #mip[mipb] = 0;mip[~mipb] = 1;
	#sitk.WriteImage(sitk.GetImageFromArray(mip), ('MASK.nii'));
      
      # return final result
      imarray = np.ma.array(imarray, mask=imarray*mipb[np.newaxis,:,:,:]) 
      imarray =imarray[np.newaxis,0:len(imarray),np.newaxis,:,:,:]       
	# plt.imshow(imarray[0,20,0,80,:,:]);
	# plt.show();
      return imarray
 
def masking_otsu(imarray):
     import numpy as np
     import scipy.ndimage as nd
     from skimage.filters import threshold_otsu, sobel 
     from skimage.morphology import opening, disk,closing,ball,erosion,remove_small_objects
     from skimage.measure import label, regionprops

	#Generate Mask
	#Input: imarray - 6D where 0,t,0,z,y,x
	#Output: masked imarray - where 0,t,0,z,y,x
     imarray=np.squeeze(imarray)
     dmip = np.mean(imarray[0:4,:,:,:],axis=0)
     mip=np.max(imarray,axis=0)-dmip;
     mip = np.squeeze(mip);
	# mip = gaussian(mip, sigma=0.8)
     thresholdOtsu = threshold_otsu(mip)
     mipb = mip > 0.75*thresholdOtsu 
     #------------------------- choose the bigest Area -------------- 
     for i in range(0,len(mipb)):
         label_img = label((mipb[i,:,:]))
         #regions = regionprops(label_img)
         labelCount = np.bincount(label_img.ravel())
         labelCount[np.argmax(labelCount)]=0
         if len(labelCount)>2:
            biggest_index = np.argmax(labelCount)
         else:
           biggest_index =-1
         mipb[i,:,:]=nd.binary_fill_holes(sobel((label_img==biggest_index)))==0         
     #--------------------------------------------------------------- 
     #------------- 3D merging     
     mipb = closing(mipb, ball(2));
     mipb = erosion(mipb, ball(4));
     #mipb = opening(mipb, ball(8));
     #mipb = erosion(mipb, ball(2)); 
     
     #----------------- save mip/mask
     #mip[mipb] = 1;mip[~mipb] = 0;
     #sitk.WriteImage(sitk.GetImageFromArray(mip), ('MASK.nii'));
           
     # return final result
     imarray = np.ma.array(imarray, mask=imarray*mipb[np.newaxis,:,:,:]) 
     imarray =imarray[np.newaxis,0:len(imarray),np.newaxis,:,:,:] 
     
	# plt.imshow(imarray[0,20,0,80,:,:]);
	# plt.show();
     return imarray