# Another way of reading xml
xmldir = data+('/*.xml');
xmlnamedir = sorted(glob.glob(xmldir));
if len(xmlnamedir)>200:
	xmlnamedir = xmlnamedir[0:200];
img, res, timeinitial, shapes, dateStr = read_xmlraw_image_func(xmlnamedir[0])
imarray = np.zeros((1,len(xmlnamedir),1,shapes[2],shapes[1],shapes[0]));
for xmlname in xmlnamedir:
	imarray[0,xmlnamedir.index(xmlname),0,:,:,:], res, timelast, shapes, dateStr = read_xmlraw_image_func(xmlname)
time = timelast - timeinitial; #total time of cine in seconds.



#From masking
	#mip=np.squeeze(np.max(imarray,axis=1)-np.squeeze(np.median(imarray[:,0:2,:,:,:,:],axis=1)));
#mip = np.squeeze(np.max(np.abs(imarray - np.mean(imarray[:,0:2,:,:,:,:],axis=1)),axis=1));
#imarray2 = resampler(imarray, imarray.shape, 0, res, [1.2,1.2,1.2]);
#imarray2 = imarray2 - np.mean(imarray2[:,0:2,:,:,:,:],axis=1);
#imarray2 = np.exp(imarray[:,0:30,:,:,:,:]/24.09).astype('uint64')

num_cores = multiprocessing.cpu_count()
yyy = Parallel(n_jobs=num_cores)(delayed(calculate_paramap)(i) for index in np.ndindex(xlist.shape[0], ylist.shape[0], zlist.shape[0]))


def calculate_paramap(index,xlist,ylist,zlist,times, windows, compression, voxelscale_res, typefit, timeconst, ):
indexinv = index[::-1]; TICz=[];params = [];popt=[];
x = xlist[index[0]];y = ylist[index[1]];z = zlist[index[2]];
k = index[2];j=index[1];i=index[0];
TICz = generate_TIC(times,windows[0,:,0,k,j,i,0,0,0,:,:,:],compression,voxelscale_res);

# Normalize array and do a bunch of checks/filters on TIC
normalizer = np.max(TICz[:,1]);
TICz[:,1] = TICz[:,1]/normalizer;
if normalizer == 0:
	#print('Skip: max of 0')
	continue;
if np.isnan(np.sum(TICz[:,1])):
	#print('Skip: nan')
	continue;
if np.isinf(np.sum(TICz[:,1])):
	#print('Skip: inf');
	continue;

# Do the fitting
try:
	params, popt, RMSE = data_fit(TICz,typefit,normalizer,timeconst);#print('before fix:');print(params);print(Rs);
	#print('Trying fit')
except RuntimeError:
	#print("Error - curve_fit failed");
	maps[:,0,0,z,y,x] = 0.001;
	continue;

# Some post-fitting filters
if RMSE > 2:
	maps[:,0,0,z,y,x] = 0.001;
	continue;
if params[params<0].any():
	maps[:,0,0,z,y,x] = 0.001;
	continue;

# Generate masks for blending - blending is (old + new)/2 if there is an old, else it's just new
#maps = generate_maps(params,maps,windSize,z,y,x);
for p in xrange(params.shape[0]):
	maps[p,0,0,z,y,x] = params[p];

return maps;

# def masking(imarray,res):
# 	#Uses Otsu thresholding method
# 	imarray2 = imarray[:,0:80,:,:,:,:];#80
# 	imarray2 = frame_diff(imarray2); imarray2[imarray2 < 10]=0;
# 	#mip = np.max(imarray2,axis=1) + np.min(imarray2,axis=1);del imarray2;
# 	mip = np.sum(imarray2,axis=1); del imarray2; 
# 	mip = mip[np.newaxis,:,:,:,:,:];
	
# 	## Thresholding
# 	thresholdOtsu = threshold_otsu(mip); print(thresholdOtsu)
# 	mip = resampler(mip, mip.shape, 0, res, [1.2,1.2,1.2]);
# 	mipb = np.squeeze(mip) < 0.35*thresholdOtsu; del mip;#.33
	
# 	## Choose the bigest Area
# 	mipb = closing(mipb, ball(1));
# 	mipb = erosion(mipb, ball(2)); 

# 	# fig = plt.figure();
# 	# ax = fig.add_subplot(111);
# 	# tracker = IndexTracker(ax, mipb);
# 	# fig.canvas.mpl_connect('scroll_event', tracker.onscroll);
# 	# plt.show();

# 	label_img = label(mipb); del mipb; 
# 	mipb = label_img==1; #mipb = 1nd.morphology.binary_fill_holes(mipb).astype(int);
# 	mipb = mipb[np.newaxis,np.newaxis,np.newaxis,:,:,:]; 
# 	mipb = resampler(mipb, mipb.shape, imarray.shape, [1.2,1.2,1.2],res); mipb = mipb >= 0.91; #.85

# 	imarray = np.ma.array(imarray, mask=imarray*mipb,fill_value=np.nan).filled();
# 	return imarray



# # CODE FOR TESTING A SINGLE TIC
# book = xlrd.open_workbook('testtic.xls')
# sheet = book.sheet_by_name('Sheet1')
# s = book.sheet_by_index(0)
# temparray = []
# for row in range(s.nrows):
#     values = []
#     for col in range(s.ncols):
#         values.append(s.cell(row,col).value)
#     temparray.append(values)
# TIC = np.asarray(temparray)
# tmppv = max(TIC[:,1])
# TIC[:,1] = TIC[:,1]/tmppv
# print(TIC);
# #print(TIC[:,0])
# params, popt, RMSE = ParaMapFunctions.data_fit(TIC,'bolus_lognormal',tmppv);print(RMSE);
# yaj = ParaMapFunctions.bolus_lognormal(TIC[:,0], popt[0], popt[1], popt[2], popt[3])
# print(yaj.shape)
# plt.plot(TIC[:,0],TIC[:,1],'x',TIC[:,0],yaj,'r-')
# plt.show()

# Mask AEK Implementation
# mip=np.max(imarray2,axis=1);
# #diffmaxip, diffminip, diffmeanip = diff_projection(imarray2[0,:,0,:,:,:],0,imarray2.shape[1]);
# mip = np.squeeze(mip);
# mipb = mip > 100;
# #ipmask = diffmaxip*mipb > 5
# selem = ball(5);
# mipb = opening(mipb,selem);


# Apply Mask
# mask = np.zeros((1,imarray2.shape[1],1,imarray2.shape[3],imarray2.shape[4],imarray2.shape[5]));
# for t in xrange(imarray2.shape[1]):
# 	mask[0,t,0,:,:,:] = mipb;	
# maskbin = mask == 0;
# imarray2 = imarray2.astype('float64');
# imarray2[maskbin] = np.nan;

#Original TIC
#  	#WILL REMOVE -Get list of frames to exclude - These frames should automatically be removed in prep_img
# FrmList = [];
# if not FrmList:
# 	DelXPosList = [];
# elif FrmList.isspace():
# 	DelXPosList = [];
# else:
# 	DelXPosList = []; 
# 	DelXPosList = [int(i) for i in FrmList.split(",")];

	#for t in xrange(0,img.shape[1]):
	# if t in DelXPosList:
	# 	continue
	# TICtime.append(times[t]); tmpwin=[];
	# tmpwin = windows[0,t,0,k,j,i,0,0,0,:,:,:];
	# TIC.append(np.exp(tmpwin[~np.isnan(tmpwin)].mean()/compression)/voxelscale_res);

# OLDER
# infoMask = dicom.read_file('Test3DCEUSData/4D/m939conBD104D.dcm')
# imMask = tff.TiffFile('Test3DCEUSData/4D/m939conBD104D.tif') 
# imarrayMask = np.reshape(imMask.asarray(), (1,1,1,z,y,x))
# orgsizes = orgimg.shape
# ot = orgsizes[1]; ox = orgsizes[5]; oy = orgsizes[4]; oz = orgsizes[3];
# sizes = img.shape
# t = sizes[1]; x = sizes[5]; y = sizes[4]; z = sizes[3];

# Apply Mask and turn all 0 values to Nan
# mask = np.zeros((1,imarray2.shape[1],1,imarray2.shape[3],imarray2.shape[4],imarray2.shape[5]))
# maxarray, minarray, meanarray = diff_projection(imarray2[0,:,0,:,:,:],0,imarray2.shape[1]);
# # mask = imarrayMask/2;
# # mask = (maxarray[0,0,0,:,:,:] < (0.01*max(maxarray[0,0,0,:,:,:])))
# maxx = (0.4*np.max(maxarray))

# # mask[0,0,0,:,:,:] = maxarray[maxarray < maxx];
# # maxx = (0.1*max(maxarray.any()))
# for t in xrange(imarray2.shape[1]):
# 	selem = disk(10)  
#      	mask[0,t,0,:,:,:] = opening(np.where(maxarray < maxx, 0, 1), selem)
# # print(mask.shape)
# maskbin = mask == 0;
# imarray2 = imarray2.astype(float)
# imarray2[maskbin] = np.nan;

# #OLD RANDOM TESTS
# view4d(PEmap,0,x,y,z);
# sitk.Show(PEimg)
# tff.imsave("PEimg.tiff",PEmap[0,0,0,:,:,:],planarconfig='planar')
# save(imarray, 'temp.dcm', info) #Using medpy
# print(niiarray.header);
# plt.imshow(img[50,:,:])
# plt.show()
# ParaMapFunctions.view4d(imarray,0,imarray.shape[1],0,imarray.shape[1],imarray.shape[1],imarray.shape[1]);
# sitk.WriteImage(sitk.GetImageFromArray(imarray[0,300,0,:,:,:]), 'xxx.nii');

# CHECK THAT IT IS A LOGNORMAL DIST.
# if (np.median(TICz[:,1]) - np.mean(TICz[:,1])) <  0.05:
# 	#Adhock check for lognormal distribution
# 	print('Not lognormal');print(np.median(TICz[:,1]) - np.mean(TICz[:,1]));
# 	continue

# #CONTINIOUS PLOTING FOR VERFICATIONS
# yaj = bolus_lognormal(TICz[:,0],popt[0],popt[1],popt[2],popt[3]);
# plt.plot(TICz[:,0],TICz[:,1],'x',TICz[:,0],yaj[:],'r-');
# plt.show();

# def masking(imarray):
# 	#Generate Mask
# 	#Input: imarray - 6D where 0,t,0,z,y,x
# 	#Output: masked imarray - where 0,t,0,z,y,x 
# 	mip=np.squeeze(np.max(imarray,axis=1)-np.mean(imarray[:,0:4,:,:,:,:],axis=1));
# 	#mip=np.divide(np.abs(mip),np.max(imarray2.astype('int16'),axis=1))*1000;

# 	#diffmaxip, diffminip, diffmeanip = diff_projection(imarray2[0,:,0,:,:,:],0,imarray2.shape[1]);
# 	# mip = np.squeeze(mip);
# 	# mip = gaussian(mip, sigma=0.8)
# 	mipb = np.exp(mip) < 1e+17; del mip;
# 	mipb = closing(mipb, ball(4));
# 	mipb = opening(mipb, ball(8));
# 	mipb = erosion(mipb, ball(10));
# 	#mip[mipb] = 1;mip[~mipb] = 0;
# 	#sitk.WriteImage(sitk.GetImageFromArray(mip), ('MASK.nii'));
# 	imarray = np.ma.array(imarray, mask=imarray*mipb[np.newaxis,np.newaxis,np.newaxis,:,:,:],fill_value=np.nan).filled();
# 	# maskedimarray = np.ma.array(imarray, mask=imarray*mipb[np.newaxis,np.newaxis,np.newaxis,:,:,:],fill_value=np.nan); 
# 	# imarray = maskedimarray.filled();

# 	# plt.imshow(imarray[0,20,0,80,:,:]);
# 	# plt.show();
# 	return imarray
