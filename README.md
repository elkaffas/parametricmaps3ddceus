# ParametricMaps3DDCEUS

This is python code used to build parameteric maps for 3D DCE-US data obtained with the X6-1 (trsfd through DNL). Code runs well on sherlock; uses an anaconda env (ParaMap.yml). Needs to run as multi-processor on multiple core for optimal data.