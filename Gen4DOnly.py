#import ParaMapFunctions as pm
import ParaMapFunctionsParallel as pm
import sys, os, glob
import nibabel as nib
from datetime import datetime
import numpy as np
#from matplotlib import pyplot as plt
#import xlrd
#import sys, os
#import SimpleITK as sitk

if __name__ == "__main__":
	# CODE ACTIVATOR
	print('Started:');print(sys.argv[1]);print(str(datetime.now()));
	path = os.path.normpath(sys.argv[1]);
	splitpath = path.split(os.sep);
	maskflag = 'yes' # flag to indicate auto-masking yes/no
	type = 'Bolus'
	fit = 'Lognormal'
	format = '3D'
	compressfactor = 24.09; #24.09; #42.98 
	print('EPIC7 Compression');
	name = splitpath[-2];
	day = splitpath[-1];day = day[0:8];
	if type == 'Bolus':
		parameters = ['PE','AUC','TP','MTT','T0']

	# Read data	
	newres = np.array([0.3, 0.3, 0.3]);
	imarray, orgres, time, imarray_org, imarray_mskd = pm.prep_img(sys.argv[1],type,format, maskflag, newres);
	print('Done 3D to 4D:');print(str(datetime.now()));

	# # Plotting function
	# fig = plt.figure()
	# ax = fig.add_subplot(111)
	# tracker = pm.IndexTracker(ax, imarray[0,30,0,:,:,:])
	# fig.canvas.mpl_connect('scroll_event', tracker.onscroll)
	# plt.show()

	# DCM pre-selected mask
	print('Applying Pre-Selected DICOM MASK')
	#dcmdirectory = '/scratch/users/elkaffas/ParaMap/Masks/';
	#dcmdirectory = '/Users/ahmedelkaffas/Documents/GoogleDrive/AEKDocs/Stanford/Project_ParaMapTexture/PythonParametricMaps/Test3DCEUSData/';
	dcmdirectory = '/mnt/c/Users/AEK/GoogleDrive/AEKDocs/Stanford/Project_ParaMapTexture/PythonParametricMaps/Test3DCEUSData/';
	#dcmdirectory = '/mnt/c/Users/AEK/Desktop/txttemp/';
	imarray_dmskd = pm.dcm_mask(imarray_org,dcmdirectory,name,day);

	# Save the 4D image without masking
	print('Saving org 4D w/o mask:');print(str(datetime.now()));
	affine = np.eye(4)
	niiarray = nib.Nifti1Image(imarray_org[0,:,0,:,:,:].astype('uint32'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [4., orgres[0], orgres[1], orgres[2], 1., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'FullOrg4D.nii')); 
	
	del imarray_org;

	#### Create linearized 4D
	imarray_lin=imarray.copy();
	imarray_lin[imarray_lin==0]= -100*compressfactor;
	imarray_lin[np.isnan(imarray_lin)]= -1000000*compressfactor;
	imarray_lin = np.exp(imarray_lin/compressfactor); # Linearized imarray - used for projections and diff.

	# Save the 4D image
	print('Saving 4D w/o mask:');print(str(datetime.now()));
	affine = np.eye(4)
	niiarray = nib.Nifti1Image(imarray_lin[0,:,0,:,:,:].astype('uint32'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [4., newres[0], newres[1], newres[2], 1., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'Full4D.nii')); 

	del imarray; del imarray_lin;

	#### Create linearized 4D
	imarray_lin=imarray_mskd;
	imarray_lin[imarray_lin==0]= -100*compressfactor;
	imarray_lin[np.isnan(imarray_lin)]= -1000000*compressfactor;
	imarray_lin = np.exp(imarray_lin/compressfactor); # Linearized imarray - used for projections and diff.

	# Save the 4D image with mask
	print('Saving 4D w/ auto mask:');print(str(datetime.now()));
	affine = np.eye(4)
	niiarray = nib.Nifti1Image(imarray_lin[0,:,0,:,:,:].astype('uint32'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [4., newres[0], newres[1], newres[2], 1., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'Full4DAutoMasked.nii')); 

	del imarray_mskd; del imarray_lin;

	#### Create linearized 4D
	imarray_lin=imarray_dmskd;
	imarray_lin[imarray_lin==0]= -100*compressfactor;
	imarray_lin[np.isnan(imarray_lin)]= -1000000*compressfactor;
	imarray_lin = np.exp(imarray_lin/compressfactor); # Linearized imarray - used for projections and diff.

	# Save the 4D image with mask
	print('Saving 4D w/ manual mask:');print(str(datetime.now()));
	affine = np.eye(4)
	niiarray = nib.Nifti1Image(imarray_lin[0,:,0,:,:,:].astype('uint32'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [4., newres[0], newres[1], newres[2], 1., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'Full4DManMasked.nii')); 

	del imarray_dmskd; del imarray_lin;

	print('Done All');print(str(datetime.now()));