#import ParaMapFunctions as pm
import ParaMapFunctionsParallel as pm
import sys, os, glob
import nibabel as nib
from datetime import datetime
import numpy as np
#from matplotlib import pyplot as plt
#import xlrd
#import sys, os
#import SimpleITK as sitk

#######Running this code ###########
# 1. remove all the *.mevis.* data in folder
# 2. run from command example: PreMask.py 411/20160509 (where the folder contains __.0.raw and __.xml data)
# Can also be run in batches in sherlock using: sbatch PreMask.batch 411 (needs PreMask.batch to be uploaded in same folder)
####################################

if __name__ == "__main__":
	# CODE ACTIVATOR
	print('Started:');print(sys.argv[1]);print(str(datetime.now()));
	path = os.path.normpath(sys.argv[1]);
	splitpath = path.split(os.sep);
	maskflag = 'no' # flag to indicate auto-masking yes/no
	type = 'Bolus'
	fit = 'Lognormal'
	format = '3D'
	compressfactor = 24.09; #24.09; #42.98 
	print('EPIC7 Compression');
	name = splitpath[-2];
	day = splitpath[-1];day = day[0:8];
	if type == 'Bolus':
		parameters = ['PE','AUC','TP','MTT','T0']

	# Read data	
	newres = np.array([0.3, 0.3, 0.3]);
	imarray, orgres, time, imarray_org = pm.prep_img(sys.argv[1],type,format, maskflag, newres);
	print('Done 3D to 4D:');print(str(datetime.now()));

	# Save the 4D image without masking
	print('Saving org 4D w/o mask:');print(str(datetime.now()));
	affine = np.eye(4)
	niiarray = nib.Nifti1Image(imarray_org[0,:,0,:,:,:].astype('uint32'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [4., orgres[0], orgres[1], orgres[2], 1., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'FullOrg4D.nii')); 
	
	del imarray_org;

	#### Create linearized 4D
	imarray_lin=imarray.copy();
	imarray_lin[imarray_lin==0]= -100*compressfactor;
	imarray_lin[np.isnan(imarray_lin)]= -1000000*compressfactor;
	imarray_lin = np.exp(imarray_lin/compressfactor); # Linearized imarray - used for projections and diff.

	## Save MIP image - Max
	print('Saving MIP:');print(str(datetime.now()));
	MIP = np.max(imarray_lin[:,:,:,:,:,:],axis=1); MIP = MIP[np.newaxis,:,:,:,:,:];
	#MIP = MIP - np.mean(imarray_lin[:,0:4,:,:,:,:],axis=1); MIP[MIP < 1]=0;
	affine = np.eye(4);
	niiarray = nib.Nifti1Image(MIP[0,0,0,:,:,:].astype('float64'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [3., newres[0], newres[1], newres[2], 0., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'MIP.nii')); 

	print('Done All');print(str(datetime.now()));