#----------------------------------------------------------------------------------
#! Macro module ParaMap
#/*!
# \file    ParaMap.py
# \author  AEK
# \date    2015-08-05
#
# 
# */
#----------------------------------------------------------------------------------

from mevis import *
from math import exp
from time import ctime
from time import sleep
from datetime import *
from os import *
import numpy as np
from pylab import *
from numpy.lib.stride_tricks import as_strided as ast

def GenParaMap ():
  #Generate Parametric Map
  print "Started Parametric Map Generation!"
  ctx.field("Bypass.noBypass").value = False;
  ctx.field("Bypass1.noBypass").value = False;
  ctx.field("ImagePropertyConvert.input0").connectFrom(ctx.field("Bypass.output0"));
  
  #Preset variables and size of imag
  #Transfer image to 4D matrix - And load a bunch of arrays; needs clean up.
  #windSize = (5,1,4);# Set to aproximatly the (Lat, Axial, Elevation) resoltuion of the image at >> Mouse Depth (4 cm)
  #stepSize = (1,1,1);
  windSize = (ctx.field("WinSizeX").value, ctx.field("WinSizeY").value, ctx.field("WinSizeZ").value);
  stepSize = (ctx.field("StepSizeX").value, ctx.field("StepSizeY").value, ctx.field("StepSizeZ").value);
  
  voxelscale = 1; #This should be a scale factor to account for voxel size variability, step size and window size > Not sure its needed here. 
  TICTime = [];
  #CompressionValue = ctx.field("Compression").value; #24.09; # Set for IU22 or Epiq7 >> Currently set to Epiq7
  #sizet = ctx.field("TicImageList.numItems").value;
  #Figure out the bounding box of the image (if there is a VOI or full image)
  #if ctx.field("VOIBoundingBoxExt.objectVoxels").value > 0:
  #  boundBoxUni = ctx.field("VOIBoundingBoxExt.boundingBox").stringValue(); 
  #  boundBoxStr = boundBoxUni.split();
  #  strtX = int(boundBoxStr[0]) - windSize[0]; endX = int(boundBoxStr[6]) + windSize[0];
  #  strtY = int(boundBoxStr[1]) - windSize[1]; endY = int(boundBoxStr[7]) + windSize[1];
  #  strtZ = int(boundBoxStr[2]) - windSize[2]; endZ = int(boundBoxStr[8]) + windSize[2];
  #else:
  #ctx.field("Mask.mode").value = "MaskedOriginal";
  #if ctx.field("Info2.voxelVolume").value > 0: # THere is a mask
  #  boundBoxUni = ctx.field("ImgBox.boundingBox").stringValue(); 
  #  boundBoxStr = boundBoxUni.split();
  #  strtX = int(boundBoxStr[0]); endX = int(boundBoxStr[6]);
  #  strtY = int(boundBoxStr[1]); endY = int(boundBoxStr[7]);
  #  strtZ = int(boundBoxStr[2]); endZ = int(boundBoxStr[8]);
  #else:
  #  ctx.field("Mask.mode").value = "Original";
  if ctx.field("ImgBox.objectVoxels").value > 0:
    boundBoxUni = ctx.field("ImgBox.boundingBox").stringValue(); 
    boundBoxStr = boundBoxUni.split();
    strtX = int(boundBoxStr[0]) - windSize[0]; endX = int(boundBoxStr[6]) + windSize[0];
    strtY = int(boundBoxStr[1]) - windSize[1]; endY = int(boundBoxStr[7]) + windSize[1];
    strtZ = int(boundBoxStr[2]) - windSize[2]; endZ = int(boundBoxStr[8]) + windSize[2];
  else:
    boundBoxUni = ctx.field("FullImgBox.boundingBox").stringValue(); 
    boundBoxStr = boundBoxUni.split();
    strtX = int(boundBoxStr[0]); endX = int(boundBoxStr[6]);
    strtY = int(boundBoxStr[1]); endY = int(boundBoxStr[7]);
    strtZ = int(boundBoxStr[2]); endZ = int(boundBoxStr[8]);
  
  print windSize; print stepSize; print boundBoxStr; #print boundBoxExt;
 
  #Set the DicomTimeTool to report the time information
  #Get the reference time for the first image in the sequence
  #Construct 4D and Create Time array
  #ctx.field("PythonArithmetic2.doubleConstant0").value = CompressionValue;
  #ctx.field("TicImageList.index").value = 0;
  #ttemp=ctx.field("DicomTagViewer.tagValue1").value;
  #ttemp1=ttemp.split('.');
  #ttemp=ttemp1[0]; ttemp2=ttemp1[1]; adate=ttemp[0]+ttemp[1]+ttemp[2]+ttemp[3]+ttemp[4]+ttemp[5]+ttemp[6]+ttemp[7];
  #atime=ttemp[8]+ttemp[9]+ttemp[10]+ttemp[11]+ttemp[12]+ttemp[13]+'.'+ttemp2;
  #ctx.field("DicomTimeTool.dicomDate1").value = adate;
  #ctx.field("DicomTimeTool.dicomTime1").value = atime;
  #for t in range(0,ctx.field("TicImageList.numItems").value):
  #  ctx.field("PythonArithmetic2.update").touch();
  #  ctx.field("TicImageList.index").value=t;
  #  ctx.field("4DCompose.add").touch();
  #  # Set the time info
  #  ttemp=ctx.field("DicomTagViewer.tagValue1").value;
  #  ttemp1=ttemp.split('.');
  #  ttemp=ttemp1[0]; ttemp2=ttemp1[1]; adate=ttemp[0]+ttemp[1]+ttemp[2]+ttemp[3]+ttemp[4]+ttemp[5]+ttemp[6]+ttemp[7];
  #  atime=ttemp[8]+ttemp[9]+ttemp[10]+ttemp[11]+ttemp[12]+ttemp[13]+'.'+ttemp2;
  #  ctx.field("DicomTimeTool.dicomDate2").value = adate;
  #  ctx.field("DicomTimeTool.dicomTime2").value = atime;
  #  TICTime.append(ctx.field("DicomTimeTool.diffSeconds").value);
  
  #Interface and grab image in img. 
  interface_rTP = ctx.module("rTP").call("getInterface")
  interface_AUC = ctx.module("AUC").call("getInterface")
  interface_TP = ctx.module("TP").call("getInterface")
  interface_MTT = ctx.module("MTT").call("getInterface")
  interface_rBV = ctx.module("rBV").call("getInterface")
  interface_rMFV = ctx.module("rMFV").call("getInterface")
  interface_rBF = ctx.module("rBF").call("getInterface")
  interface_PE = ctx.module("PE").call("getInterface")
  
  #Img4D = ctx.field("Bypass.output0").image();
  Img4D = ctx.field("Bypass.output0").image();
  extent = Img4D.imageExtent();
  fullImg = Img4D.getTile((0,0,0,0,0,0), extent);
  img = np.zeros((1,fullImg.shape[1],1,endZ-strtZ,endY-strtY,endX-strtX));
  img = fullImg[:,:,:,strtZ:endZ,strtY:endY,strtX:endX];
  #img = np.zeros((1,fullImg.shape[1],1,fullImg.shape[3],fullImg.shape[4],fullImg.shape[5]));
  print Img4D.maxVoxelValue();
  ctx.field("ImagePropertyConvert.input0").disconnect();
  #ctx.field("Bypass.noBypass").value = True;
  
  # Load mask
  Mask4D = ctx.field("Bypass1.output0").image();
  extent2 = Mask4D.imageExtent();
  fullMask = Mask4D.getTile((0,0,0,0,0,0), extent2);
  mask = np.zeros((1,fullMask.shape[1],1,endZ-strtZ,endY-strtY,endX-strtX));
  mask = fullMask[:,:,:,strtZ:endZ,strtY:endY,strtX:endX];
  
  #mask image to add nans
  mask = mask/2;
  maskbin = mask == 0;
  img = img.astype(float)
  img[maskbin] = np.nan;
  
  #Parameters to be calculated - will create the same for DR parameters. These are without fitting
  tempImg_rTP = np.zeros((1,1,1,img.shape[3],img.shape[4],img.shape[5]));
  finalImg_rTP = np.zeros((1,1,1,fullImg.shape[3],fullImg.shape[4],fullImg.shape[5]));
  tempImg_TP = np.zeros((1,1,1,img.shape[3],img.shape[4],img.shape[5]));
  finalImg_TP = np.zeros((1,1,1,fullImg.shape[3],fullImg.shape[4],fullImg.shape[5]));
  tempImg_PE = np.zeros((1,1,1,img.shape[3],img.shape[4],img.shape[5]));
  finalImg_PE = np.zeros((1,1,1,fullImg.shape[3],fullImg.shape[4],fullImg.shape[5]));
  tempImg_AUC = np.zeros((1,1,1,img.shape[3],img.shape[4],img.shape[5]));
  finalImg_AUC = np.zeros((1,1,1,fullImg.shape[3],fullImg.shape[4],fullImg.shape[5]));
  tempImg_MTT = np.zeros((1,1,1,img.shape[3],img.shape[4],img.shape[5]));
  finalImg_MTT = np.zeros((1,1,1,fullImg.shape[3],fullImg.shape[4],fullImg.shape[5]));
  tempImg_rBV = np.zeros((1,1,1,img.shape[3],img.shape[4],img.shape[5]));
  finalImg_rBV = np.zeros((1,1,1,fullImg.shape[3],fullImg.shape[4],fullImg.shape[5]));
  tempImg_rMFV = np.zeros((1,1,1,img.shape[3],img.shape[4],img.shape[5]));
  finalImg_rMFV = np.zeros((1,1,1,fullImg.shape[3],fullImg.shape[4],fullImg.shape[5]));
  tempImg_rBF = np.zeros((1,1,1,img.shape[3],img.shape[4],img.shape[5]));
  finalImg_rBF = np.zeros((1,1,1,fullImg.shape[3],fullImg.shape[4],fullImg.shape[5]));
  
  print windSize; print stepSize; print extent;
  print img.shape;print fullImg.shape;
  print strtX; print strtY; print strtZ; print endX; print endY; print endZ;
  print boundBoxUni; print boundBoxStr;print img.max();print img.shape;
  
  #Make my windows
  windows = sliding_window(img,(1,1,1,windSize[2],windSize[1],windSize[0]),(1,1,1,stepSize[2],stepSize[1],stepSize[0]), False); 
  
  #Check that all is good... 
  print windows.shape;
  print extent;print img.shape;print fullImg.shape;
  print img.max();#print Img4D.maxVoxelValue();
  print img.shape;
  print boundBoxUni; print boundBoxStr;
  
  #Get list of frames to exclude
  FrmList = ctx.field("ExcludeFrames").stringValue();
  if not FrmList:
    DelXPosList = [];
  elif FrmList.isspace():
    DelXPosList = [];
  else:
    DelXPosList = []; 
    DelXPosList = [int(i) for i in FrmList.split(",")];
  
  #Build array of windowed values > This nested for loop coul dbe re-written for speed, maybe using numpy mehtods. 
  i=0;
  for x in xrange(int(floor(windSize[0]/2)),int(img.shape[5]-floor(windSize[0]/2)),stepSize[0]):
    j=0;
    for y in xrange(int(floor(windSize[1]/2)),int(img.shape[4]-floor(windSize[1]/2)),stepSize[1]):
      k=0;
      for z in xrange(int(floor(windSize[2]/2)),int(img.shape[3]-floor(windSize[2]/2)),stepSize[2]):
        TIC = [];TICtime = []; ddd = [];#Restart the TIC each time
        
        #Get the TIC for the specific window-voxel - This is where I could really speed things up using Numpy.
        for t in xrange(0,img.shape[1]):
          if t in DelXPosList:
            continue;
          TICtime.append(t);
          zzzz = windows[0,t,0,k,j,i,0,0,0,:,:,:];
          TIC.append(zzzz[~np.isnan(zzzz)].mean());
          #TIC.append(mean(windows[0,t,0,k,j,i,0,0,0,:,:,:]));
        
        TICz = np.array([TICtime,TIC]);
        ctx.field("CurveImport.clear").touch();
        ctx.field("CurveToMatlab.restartMatlab").touch(); 
        ctx.field("CurveImport.curveTable").setStringValue('\n'.join(', '.join(str(cell) for cell in row) for row in TICz.transpose()));
        ctx.field("CurveImport.update").touch();
        ctx.field("CurveToMatlab.update").touch(); 
        
        #auc =  ctx.field("CurveToMatlab.scalar0").value;
        #mu =  ctx.field("CurveToMatlab.scalar1").value;
        #sigma =  ctx.field("CurveToMatlab.scalar2").value;
        #strtT =  ctx.field("CurveToMatlab.scalar3").value;
        #pv =  ctx.field("CurveToMatlab.scalar4").value;
        Bolus = ctx.field("CurveToMatlab.vector0").vectorValue()
        BolusExtra = ctx.field("CurveToMatlab.vector2").vectorValue()
        Infusion = ctx.field("CurveToMatlab.vector1").vectorValue()
        PE = Bolus[0];
        AUC = Bolus[1];
        TP = Bolus[2];
        MTT = Bolus[3];
        rTP = BolusExtra[0]+TP;
        rBV = Infusion[0];
        rMFV = Infusion[1];
        rBF = Infusion[2];
        #print rBV;print PE;a
        
        #print mu;print auc;print sigma;print strtT;print tmppv;print pv;
        #mtt=exp(mu+sigma*sigma/2); tp = exp(mu-sigma*sigma); pv=clognormal (tp+tic(1,1));rbf=pv/auc; rbv = rbf*mtt;
        
        #Generate window array for filing:
        #zz = np.array((z-int(floor(windSize[2]/2))):(z+int(floor(windSize[2]/2))));
        #zz = np.array(z:(z+stepSize[2]));
        #zz = np.array((z-stepSize[2]):(z+stepSize[2]))
        #yy
        #yy
        #yy
        #xx
        #xx
        #xx
        
        #1. This files in voxel from z to step size - no blending
        #tempImg_rTP[0,0,0,z:(z+stepSize[2]),y:(y+stepSize[1]),x:(x+stepSize[0])] = rTP; 
        #tempImg_TP[0,0,0,z:(z+stepSize[2]),y:(y+stepSize[1]),x:(x+stepSize[0])] = TP;
        #tempImg_PE[0,0,0,z:(z+stepSize[2]),y:(y+stepSize[1]),x:(x+stepSize[0])] = PE;
        #tempImg_AUC[0,0,0,z:(z+stepSize[2]),y:(y+stepSize[1]),x:(x+stepSize[0])] = AUC;
        #tempImg_MTT[0,0,0,z:(z+stepSize[2]),y:(y+stepSize[1]),x:(x+stepSize[0])] = MTT;   
        #tempImg_rBV[0,0,0,z:(z+stepSize[2]),y:(y+stepSize[1]),x:(x+stepSize[0])] = rBV;
        #tempImg_rMFV[0,0,0,z:(z+stepSize[2]),y:(y+stepSize[1]),x:(x+stepSize[0])] = rMFV;
        #tempImg_rBF[0,0,0,z:(z+stepSize[2]),y:(y+stepSize[1]),x:(x+stepSize[0])] = rBF;
        
        #->Generate masks for blending - blending is (old + new)/2 if there is an old, else it's just new
        ddd1 = tempImg_PE[0,0,0,(z-int(floor(windSize[2]/2))):(z+int(floor(windSize[2]/2))),(y-int(floor(windSize[1]/2))):(y+int(floor(windSize[1]/2))),(x-int(floor(windSize[0]/2))):(x+int(floor(windSize[0]/2)))]
        ddd2 = np.where(ddd1 > 0,2,1); # masks resulting array (dd2) with 1 where it is 0 and 2 where more

        #2. blending within the windows instead of space between step sizes. Blending takes average of the two values set to be at the same location > Could be implemented better - maybe take median?
        tempImg_PE[0,0,0,(z-int(floor(windSize[2]/2))):(z+int(floor(windSize[2]/2))),(y-int(floor(windSize[1]/2))):(y+int(floor(windSize[1]/2))),(x-int(floor(windSize[0]/2))):(x+int(floor(windSize[0]/2)))] = (tempImg_PE[0,0,0,(z-int(floor(windSize[2]/2))):(z+int(floor(windSize[2]/2))),(y-int(floor(windSize[1]/2))):(y+int(floor(windSize[1]/2))),(x-int(floor(windSize[0]/2))):(x+int(floor(windSize[0]/2)))]+PE)/ddd2;
        tempImg_AUC[0,0,0,(z-int(floor(windSize[2]/2))):(z+int(floor(windSize[2]/2))),(y-int(floor(windSize[1]/2))):(y+int(floor(windSize[1]/2))),(x-int(floor(windSize[0]/2))):(x+int(floor(windSize[0]/2)))] = (tempImg_AUC[0,0,0,(z-int(floor(windSize[2]/2))):(z+int(floor(windSize[2]/2))),(y-int(floor(windSize[1]/2))):(y+int(floor(windSize[1]/2))),(x-int(floor(windSize[0]/2))):(x+int(floor(windSize[0]/2)))]+AUC)/ddd2;
        tempImg_TP[0,0,0,(z-int(floor(windSize[2]/2))):(z+int(floor(windSize[2]/2))),(y-int(floor(windSize[1]/2))):(y+int(floor(windSize[1]/2))),(x-int(floor(windSize[0]/2))):(x+int(floor(windSize[0]/2)))] = (tempImg_TP[0,0,0,(z-int(floor(windSize[2]/2))):(z+int(floor(windSize[2]/2))),(y-int(floor(windSize[1]/2))):(y+int(floor(windSize[1]/2))),(x-int(floor(windSize[0]/2))):(x+int(floor(windSize[0]/2)))]+TP)/ddd2;
        tempImg_MTT[0,0,0,(z-int(floor(windSize[2]/2))):(z+int(floor(windSize[2]/2))),(y-int(floor(windSize[1]/2))):(y+int(floor(windSize[1]/2))),(x-int(floor(windSize[0]/2))):(x+int(floor(windSize[0]/2)))] = (tempImg_MTT[0,0,0,(z-int(floor(windSize[2]/2))):(z+int(floor(windSize[2]/2))),(y-int(floor(windSize[1]/2))):(y+int(floor(windSize[1]/2))),(x-int(floor(windSize[0]/2))):(x+int(floor(windSize[0]/2)))]+MTT)/ddd2;
        tempImg_rTP[0,0,0,(z-int(floor(windSize[2]/2))):(z+int(floor(windSize[2]/2))),(y-int(floor(windSize[1]/2))):(y+int(floor(windSize[1]/2))),(x-int(floor(windSize[0]/2))):(x+int(floor(windSize[0]/2)))] = (tempImg_rTP[0,0,0,(z-int(floor(windSize[2]/2))):(z+int(floor(windSize[2]/2))),(y-int(floor(windSize[1]/2))):(y+int(floor(windSize[1]/2))),(x-int(floor(windSize[0]/2))):(x+int(floor(windSize[0]/2)))]+rTP)/ddd2;

        #3. blends - step size to + step size
        #create mask for blending - based on averaging old value and new value. 
        #ddd1 = tempImg_rTP[0,0,0,(z-stepSize[2]):(z+stepSize[2]),(y-stepSize[1]):(y+stepSize[1]),(x-stepSize[0]):(x+stepSize[0])]
        #ddd2 = np.where(ddd1 > 0,2,1);
        ##blend the values
        #tempImg_rTP[0,0,0,(z-stepSize[2]):(z+stepSize[2]),(y-stepSize[1]):(y+stepSize[1]),(x-stepSize[0]):(x+stepSize[0])] = (tempImg_rTP[0,0,0,(z-stepSize[2]):(z+stepSize[2]),(y-stepSize[1]):(y+stepSize[1]),(x-stepSize[0]):(x+stepSize[0])]+rTP)/ddd2;        
        #tempImg_TP[0,0,0,(z-stepSize[2]):(z+stepSize[2]),(y-stepSize[1]):(y+stepSize[1]),(x-stepSize[0]):(x+stepSize[0])] = (tempImg_TP[0,0,0,(z-stepSize[2]):(z+stepSize[2]),(y-stepSize[1]):(y+stepSize[1]),(x-stepSize[0]):(x+stepSize[0])]+TP)/ddd2;
        #tempImg_PE[0,0,0,(z-stepSize[2]):(z+stepSize[2]),(y-stepSize[1]):(y+stepSize[1]),(x-stepSize[0]):(x+stepSize[0])] = (tempImg_PE[0,0,0,(z-stepSize[2]):(z+stepSize[2]),(y-stepSize[1]):(y+stepSize[1]),(x-stepSize[0]):(x+stepSize[0])]+rPE)/ddd2;
        #tempImg_AUC[0,0,0,(z-stepSize[2]):(z+stepSize[2]),(y-stepSize[1]):(y+stepSize[1]),(x-stepSize[0]):(x+stepSize[0])] = (tempImg_AUC[0,0,0,(z-stepSize[2]):(z+stepSize[2]),(y-stepSize[1]):(y+stepSize[1]),(x-stepSize[0]):(x+stepSize[0])]+AUC)/ddd2;
        #tempImg_MTT[0,0,0,(z-stepSize[2]):(z+stepSize[2]),(y-stepSize[1]):(y+stepSize[1]),(x-stepSize[0]):(x+stepSize[0])] = (tempImg_MTT[0,0,0,(z-stepSize[2]):(z+stepSize[2]),(y-stepSize[1]):(y+stepSize[1]),(x-stepSize[0]):(x+stepSize[0])]+MTT)/ddd2;   
        #tempImg_rBV[0,0,0,(z-stepSize[2]):(z+stepSize[2]),(y-stepSize[1]):(y+stepSize[1]),(x-stepSize[0]):(x+stepSize[0])] = (tempImg_rBV[0,0,0,(z-stepSize[2]):(z+stepSize[2]),(y-stepSize[1]):(y+stepSize[1]),(x-stepSize[0]):(x+stepSize[0])]+rBV)/ddd2;
        #tempImg_rMFV[0,0,0,(z-stepSize[2]):(z+stepSize[2]),(y-stepSize[1]):(y+stepSize[1]),(x-stepSize[0]):(x+stepSize[0])] = (tempImg_rMFV[0,0,0,(z-stepSize[2]):(z+stepSize[2]),(y-stepSize[1]):(y+stepSize[1]),(x-stepSize[0]):(x+stepSize[0])]+rMFV)/ddd2;
        #tempImg_rBF[0,0,0,(z-stepSize[2]):(z+stepSize[2]),(y-stepSize[1]):(y+stepSize[1]),(x-stepSize[0]):(x+stepSize[0])] = (tempImg_rBF[0,0,0,(z-stepSize[2]):(z+stepSize[2]),(y-stepSize[1]):(y+stepSize[1]),(x-stepSize[0]):(x+stepSize[0])]+rBF)/ddd2;

        k=k+1
      j=j+1
      print PE;
    i=i+1
  
  #Return final image
  finalImg_rTP[0,0,0,strtZ:endZ, strtY:endY, strtX:endX] = tempImg_rTP;
  finalImg_TP[0,0,0,strtZ:endZ, strtY:endY, strtX:endX] = tempImg_TP;
  finalImg_AUC[0,0,0,strtZ:endZ, strtY:endY, strtX:endX] = tempImg_AUC;
  finalImg_PE[0,0,0,strtZ:endZ, strtY:endY, strtX:endX] = tempImg_PE;
  finalImg_MTT[0,0,0,strtZ:endZ, strtY:endY, strtX:endX] = tempImg_MTT;
  finalImg_rBV[0,0,0,strtZ:endZ, strtY:endY, strtX:endX] = tempImg_rBV;
  finalImg_rMFV[0,0,0,strtZ:endZ, strtY:endY, strtX:endX] = tempImg_rMFV;
  finalImg_rBF[0,0,0,strtZ:endZ, strtY:endY, strtX:endX] = tempImg_rBF;
  #finalImg_rTP[np.isnan(finalImg_rTP)] = 0;
  #finalImg_TP[np.isnan(finalImg_TP)] = 0;
  #finalImg_AUC[np.isnan(finalImg_AUC)] = 0;
  #finalImg_PE[np.isnan(finalImg_PE)] = 0;
  #finalImg_MTT[np.isnan(finalImg_MTT)] = 0;
  #finalImg_rBV[np.isnan(finalImg_rBV)] = 0;
  #finalImg_rMFV[np.isnan(finalImg_rMFV)] = 0;
  #finalImg_rBF[np.isnan(finalImg_rBF)] = 0;
  interface_rTP.setImage(finalImg_rTP);
  interface_TP.setImage(finalImg_TP);
  interface_AUC.setImage(finalImg_AUC);
  interface_PE.setImage(finalImg_PE);
  interface_MTT.setImage(finalImg_MTT);
  interface_rBV.setImage(finalImg_rBV);
  interface_rMFV.setImage(finalImg_rMFV);
  interface_rBF.setImage(finalImg_rBF);
  
  print "Finished Parametric Map Generation!";

def norm_shape(shape):
  '''
  Normalize numpy array shapes so they're always expressed as a tuple, 
  even for one-dimensional shapes.
   
  Parameters
      shape - an int, or a tuple of ints
   
  Returns
      a shape tuple
  '''
  try:
      i = int(shape)
      return (i,)
  except TypeError:
      # shape was not a number
      pass
  
  try:
      t = tuple(shape)
      return t
  except TypeError:
      # shape was not iterable
      pass
   
  raise TypeError('shape must be an int, or a tuple of ints')

def sliding_window(a,ws,ss = None,flatten = True):
  '''
  Return a sliding window over a in any number of dimensions
   
  Parameters:
      a  - an n-dimensional numpy array
      ws - an int (a is 1D) or tuple (a is 2D or greater) representing the size 
           of each dimension of the window
      ss - an int (a is 1D) or tuple (a is 2D or greater) representing the 
           amount to slide the window in each dimension. If not specified, it
           defaults to ws.
      flatten - if True, all slices are flattened, otherwise, there is an 
                extra dimension for each dimension of the input.
   
  Returns
      an array containing each n-dimensional window from a
  '''
   
  if None is ss:
      # ss was not provided. the windows will not overlap in any direction.
      ss = ws
  ws = norm_shape(ws)
  ss = norm_shape(ss)
   
  # convert ws, ss, and a.shape to numpy arrays so that we can do math in every 
  # dimension at once.
  ws = np.array(ws)
  ss = np.array(ss)
  shape = np.array(a.shape)
   
   
  # ensure that ws, ss, and a.shape all have the same number of dimensions
  ls = [len(shape),len(ws),len(ss)]
  if 1 != len(set(ls)):
      raise ValueError(\
      'a.shape, ws and ss must all have the same length. They were %s' % str(ls))
   
  # ensure that ws is smaller than a in every dimension
  if np.any(ws > shape):
      raise ValueError(\
      'ws cannot be larger than a in any dimension a.shape was %s and ws was %s' % (str(a.shape),str(ws)))
   
  # how many slices will there be in each dimension?
  newshape = norm_shape(((shape - ws) // ss) + 1)
  # the shape of the strided array will be the number of slices in each dimension
  # plus the shape of the window (tuple addition)
  newshape += norm_shape(ws)
  # the strides tuple will be the array's strides multiplied by step size, plus
  # the array's strides (tuple addition)
  newstrides = norm_shape(np.array(a.strides) * ss) + a.strides
  strided = ast(a,shape = newshape,strides = newstrides)
  if not flatten:
      return strided
   
  # Collapse strided so that it has one more dimension than the window.  I.e.,
  # the new array is a flat list of slices.
  meat = len(ws) if ws.shape else 0
  firstdim = (np.product(newshape[:-meat]),) if ws.shape else ()
  dim = firstdim + (newshape[-meat:])
  # remove any dimensions with size 1
  dim = filter(lambda i : i != 1,dim)
  
  return strided.reshape(dim);