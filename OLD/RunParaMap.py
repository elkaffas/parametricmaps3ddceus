import sys
import os
import ParaMapFunctions
import xlrd
import numpy as np
from matplotlib import pyplot as plt
import SimpleITK as sitk
import nibabel as nib
from datetime import datetime

if __name__ == "__main__":
	# CODE ACTIVATOR
	print('Started:');print(sys.argv[1]);print(str(datetime.now()));
	type = 'bolus'
	format = '3D'
	if type == 'bolus':
		parameters = ['pe','auc','tp','mtt']

	# Read data	
	imarray, res, time = ParaMapFunctions.prep_img(sys.argv[1],type,format);
	print('Done 3D to 4D:');print(str(datetime.now()));

	# plt.imshow(imarray[0,20,0,80,:,:]);
	# plt.show();

	# Save the 4D image
	# affine = np.eye(4)
	# niiarray = nib.Nifti1Image(imarray[0,0:80,0,:,:,:].astype('uint8'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	# niiarray.header['pixdim'] = [4., res[0], res[1], res[2], 1., 0., 0., 0.];
	# nib.save(niiarray, 'Full4D.nii');

	# Run para map gen
	print('Start Maps:');print(str(datetime.now()))
	res, maps = ParaMapFunctions.paramap(imarray, res, np.array([0.4, 0.4, 0.4]), time);print(maps.shape);
	maps = maps.astype('float64');
	print('Done Maps:');print(str(datetime.now()));print('Saving...');

	# Save parametric maps
	for p in xrange(len(parameters)):
		printname = 'Test' + parameters[p] + '.nii';# FIX NAME SAVING METHOD
		#ParaMapFunctions.view4d(maps,p,0,0,maps.shape[3],maps.shape[4],maps.shape[5]);
		sitk.WriteImage(sitk.GetImageFromArray(maps[p,0,0,:,:,:]), ('A'+printname)); 
		affine = np.eye(4)
		niiarray = nib.Nifti1Image(maps[p,0,0,:,:,:],affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
		niiarray.header['pixdim'] = [3., res[0], res[1], res[2], 0., 0., 0., 0.];
		nib.save(niiarray, ('T'+ printname));
	print('Done:');print(str(datetime.now()))
