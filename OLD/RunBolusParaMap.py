#import ParaMapFunctions as pm
import ParaMapFunctionsParallel as pm
import sys, os, glob
import nibabel as nib
from datetime import datetime
import numpy as np
#from matplotlib import pyplot as plt
#import xlrd
#import sys, os
#import SimpleITK as sitk

if __name__ == "__main__":
	# CODE ACTIVATOR
	print('Started:');print(sys.argv[1]);print(str(datetime.now()));
	path = os.path.normpath(sys.argv[1]);
	splitpath = path.split(os.sep);
	maskflag = 'no' # flag to indicate auto-masking yes/no
	type = 'Bolus'
	fit = 'Lognormal'
	format = '3D'
	compressfactor = 42.98; #24.09; #42.98 
	print('iU22 Compression');
	name = splitpath[-2];
	day = splitpath[-1];day = day[0:8];
	if type == 'Bolus':
		parameters = ['PE','AUC','TP','MTT','T0']

	# Read data	
	newres = np.array([0.3, 0.3, 0.3]);
	imarray, orgres, time, imarray_org = pm.prep_img(sys.argv[1],type,format, maskflag, newres);
	print('Done 3D to 4D:');print(str(datetime.now()));

	# # Plotting function
	# fig = plt.figure()
	# ax = fig.add_subplot(111)
	# tracker = pm.IndexTracker(ax, imarray[0,30,0,:,:,:])
	# fig.canvas.mpl_connect('scroll_event', tracker.onscroll)
	# plt.show()

	# DCM pre-selected mask
	print('Applying Pre-Selected DICOM MASK')
	dcmdirectory = '/scratch/users/elkaffas/ParaMap/Masks/';
	#dcmdirectory = '/Users/ahmedelkaffas/Documents/GoogleDrive/AEKDocs/Stanford/Project_ParaMapTexture/PythonParametricMaps/Test3DCEUSData/';
	#dcmdirectory = '/mnt/c/Users/AEK/GoogleDrive/AEKDocs/Stanford/Project_ParaMapTexture/PythonParametricMaps/Test3DCEUSData/';
	#dcmdirectory = '/mnt/c/Users/AEK/Desktop/txttemp/';
	imarray = pm.dcm_mask(imarray_org,dcmdirectory,name,day);

	# Run avg fitting
	print('Start Avg Fit:');print(str(datetime.now()))
	params = pm.avgfit(imarray, newres, time, type + fit, compressfactor);
	print('Done Avg Fit:');print(str(datetime.now()));
	print(params);
	
	# Save avg fit data
	print('Saving Avg Fits...');print(str(datetime.now()));
	np.savetxt(name + 'd' + day + type + '.txt', params, delimiter=',')

	# Run para map gen
	print('Start Maps:');print(str(datetime.now()))
	maps = pm.paramap(imarray, newres, time, type + fit, compressfactor);
	#maps = maps.astype('float64');
	print('Done Maps:');print(str(datetime.now()));print('Saving...');

	# Save parametric maps
	for p in xrange(len(parameters)):
		printname = name + 'd' + day + type + parameters[p] + '.nii';# FIX NAME SAVING METHOD
		#ParaMapFunctions.view4d(maps,p,0,0,maps.shape[3],maps.shape[4],maps.shape[5]);
		#sitk.WriteImage(sitk.GetImageFromArray(maps[p,0,0,:,:,:]), ('A'+printname)); 
		affine = np.eye(4)
		niiarray = nib.Nifti1Image(maps[p,0,0,:,:,:].astype('float64'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
		niiarray.header['pixdim'] = [3., newres[0], newres[1], newres[2], 0., 0., 0., 0.];
		nib.save(niiarray, (printname));
	del maps;
	print('Done Maps');print(str(datetime.now()));

	# Save the 4D image without masking
	print('Saving 4D w/o mask:');print(str(datetime.now()));
	affine = np.eye(4)
	niiarray = nib.Nifti1Image(imarray_org[0,:,0,:,:,:].astype('uint32'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [4., newres[0], newres[1], newres[2], 1., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'Full4D.nii')); 
	
	del imarray_org;

	# Save the 4D image with mask
	print('Saving 4D w/ mask:');print(str(datetime.now()));
	affine = np.eye(4)
	niiarray = nib.Nifti1Image(imarray[0,:,0,:,:,:].astype('uint32'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [4., newres[0], newres[1], newres[2], 1., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'Full4DMasked.nii')); 

	# Create linearized 4D
	imarray_lin=imarray;
	imarray_lin[imarray_lin==0]= -100*compressfactor;
	imarray_lin[np.isnan(imarray_lin)]= -1000000*compressfactor;
	imarray_lin = np.exp(imarray_lin/compressfactor); # Linearized imarray - used for projections and diff.

	## Save 4D MIP image - Max
	print('Saving MIP:');print(str(datetime.now()));
	MIP = np.max(imarray_lin[:,:,:,:,:,:],axis=1); MIP = MIP[np.newaxis,:,:,:,:,:];
	#MIP = MIP - np.mean(imarray_lin[:,0:4,:,:,:,:],axis=1); MIP[MIP < 1]=0;
	affine = np.eye(4);
	niiarray = nib.Nifti1Image(MIP[0,0,0,:,:,:].astype('float64'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [3., newres[0], newres[1], newres[2], 0., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'MIP.nii')); 

	## Save 4D AIP image - Avg over first hundred frames... 
	print('Saving AIP:');print(str(datetime.now()));
	AIP = np.mean(imarray_lin[:,:100,:,:,:,:],axis=1); AIP = AIP[np.newaxis,:,:,:,:,:];
	#AIP = AIP - np.mean(imarray_lin[:,0:4,:,:,:,:],axis=1); AIP[AIP < 1]=0;
	affine = np.eye(4);
	niiarray = nib.Nifti1Image(AIP[0,0,0,:,:,:].astype('float64'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [3., newres[0], newres[1], newres[2], 0., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'AIP.nii'));

	## Save 4D SIP image - Std 
	print('Saving SIP:');print(str(datetime.now()));
	SIP = np.std(imarray_lin[:,:,:,:,:,:],axis=1); SIP = SIP[np.newaxis,:,:,:,:,:];
	affine = np.eye(4);
	niiarray = nib.Nifti1Image(SIP[0,0,0,:,:,:].astype('float64'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [3., newres[0], newres[1], newres[2], 0., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'SIP.nii'));

	## Save 4D SumDiff image - Sum of Differences over first 80 frames
	print('Saving SumDiff:');print(str(datetime.now()));
	diffs = pm.frame_diff(imarray_lin[:,:80,:,:,:,:]); 
	SumDiff = np.sum(diffs,axis=1); 
	SumDiff = SumDiff[np.newaxis,:,:,:,:,:];#SumDiff[SumDiff < 1]=0;
	#SumDiff = SumDiff - np.mean(SumDiff[:,0:4,:,:,:,:],axis=1); SumDiff[SumDiff < 1]=0;
	affine = np.eye(4);
	niiarray = nib.Nifti1Image(SumDiff[0,0,0,:,:,:].astype('float64'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [3., newres[0], newres[1], newres[2], 0., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'SumDiff.nii'));

	## Save 4D StdDiff image - Std of Differences over first 80 frames
	print('Saving StdDiff:');print(str(datetime.now()));
	#diffs = pm.frame_diff(imarray);
	StdDiff = np.std(diffs,axis=1); 
	StdDiff = StdDiff[np.newaxis,:,:,:,:,:];#StdDiff[StdDiff < 1]=0;
	#StdDiff = StdDiff - np.mean(StdDiff[:,0:4,:,:,:,:],axis=1); StdDiff[StdDiff < 1]=0;
	affine = np.eye(4);
	niiarray = nib.Nifti1Image(StdDiff[0,0,0,:,:,:].astype('float64'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [3., newres[0], newres[1], newres[2], 0., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'StdDiff.nii'));

	## Save 4D MaxDiff image - Max of Differences over first 80 frames
	print('Saving MaxDiff:');print(str(datetime.now()));
	#diffs = pm.frame_diff(imarray);
	MaxDiff = np.max(diffs,axis=1); 
	MaxDiff = MaxDiff[np.newaxis,:,:,:,:,:]; #MaxDiff[MaxDiff < 1]=0;
	#MaxDiff = MaxDiff - np.mean(MaxDiff[:,0:4,:,:,:,:],axis=1); MaxDiff[MaxDiff < 1]=0;
	affine = np.eye(4);
	niiarray = nib.Nifti1Image(MaxDiff[0,0,0,:,:,:].astype('float64'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [3., newres[0], newres[1], newres[2], 0., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'MaxDiff.nii'));

	## Save 4D MaxDiff image - Min of Differences over first 80 frames
	print('Saving MinDiff:');print(str(datetime.now()));
	#diffs = pm.frame_diff(imarray);
	MinDiff = np.min(diffs,axis=1); 
	MinDiff = MinDiff[np.newaxis,:,:,:,:,:]; #MaxDiff[MaxDiff < 1]=0;
	#MaxDiff = MaxDiff - np.mean(MaxDiff[:,0:4,:,:,:,:],axis=1); MaxDiff[MaxDiff < 1]=0;
	affine = np.eye(4);
	niiarray = nib.Nifti1Image(MinDiff[0,0,0,:,:,:].astype('float64'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [3., newres[0], newres[1], newres[2], 0., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'MinDiff.nii'));

	## Save 4D AvgDiff image - Avg of Differences over first 40 frames
	print('Saving AvgDiff:');print(str(datetime.now()));
	diffs = pm.frame_diff(imarray_lin[:,:40,:,:,:,:]); 
	AvgDiff = np.mean(diffs,axis=1); 
	AvgDiff = AvgDiff[np.newaxis,:,:,:,:,:];#AvgDiff[MaxDiff < 1]=0;
	#MaxDiff = MaxDiff - np.mean(MaxDiff[:,0:4,:,:,:,:],axis=1); MaxDiff[MaxDiff < 1]=0;
	affine = np.eye(4);
	niiarray = nib.Nifti1Image(AvgDiff[0,0,0,:,:,:].astype('float64'),affine);#NEED TO FLIP THESE IN X,Y,Z,T for quick mevis read. 
	niiarray.header['pixdim'] = [3., newres[0], newres[1], newres[2], 0., 0., 0., 0.];
	nib.save(niiarray, (name + 'd' + day + type + 'AvgDiff.nii'));

	## Clear memory by deleting non-necessary images after saving them. 
	del AvgDiff, MinDiff, MaxDiff, StdDiff, SumDiff, SIP, imarray_lin, MIP, AIP;

	print('Done All');print(str(datetime.now()));