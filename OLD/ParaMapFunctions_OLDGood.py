from __future__ import print_function
import cv2
import PIL
from PIL import Image
from resizeimage import resizeimage
import numpy as np
import scipy as sp 
import scipy.misc
import tifffile as tff
#import libtiff
import dicom 
from math import exp
from math import floor
from math import ceil
import math
from time import ctime
from time import sleep
from datetime import datetime
#from pylab import *
from numpy.lib.stride_tricks import as_strided as ast
#from medpy.io import load, save
import SimpleITK as sitk
from scipy import optimize
from scipy.optimize import curve_fit
from lmfit import  Model 
import skimage.transform
import xml.etree.ElementTree as ET
from matplotlib import pyplot as plt
from sklearn.metrics import mean_squared_error
import os
import glob
import sys
from skimage.morphology import opening, ball, disk, closing, dilation, erosion
from skimage.filters import gaussian
from skimage.measure import label, regionprops
from skimage.viewer import ImageViewer
from scipy import stats

def prep_img(data, type, format):
	#Code Does: 
	#1 read in a 4D image or a set of 3D images
	#2 Find flashes if any
	#3 Apply mask

	# 1. Loading DATA
	if format == '4D':
		datadicom = data + ('.dcm');
		datatif = data + ('.tif');
		info = dicom.read_file(datadicom)
		zres = info.SpacingBetweenSlices; 
		yres = info.PixelSpacing[0]; 
		xres = info.PixelSpacing[1]; 
		res=np.array([xres,yres,zres])
		im = tff.TiffFile(datatif) 
		t = info.NumberOfTemporalPositions; time =0;
		x = im.asarray().shape[2]
		y = im.asarray().shape[1]
		z = im.asarray().shape[0]/t
		#Shape image into typical mevislab/itk format
		imarray = np.reshape(im.asarray(), (1,t,1,z,y,x))
	elif format == '3D':
		xmldir = data+('/*.xml');
		xmlnamedir = glob.glob(xmldir);
		img, res, time, shapes, dateStr = read_xmlraw_image_func(xmlnamedir[0])
		imarray = np.zeros((1,len(xmlnamedir),1,shapes[2],shapes[1],shapes[0]));
		for xmlname in xmlnamedir:
			imarray[0,xmlnamedir.index(xmlname),0,:,:,:], res, time, shapes, dateStr = read_xmlraw_image_func(xmlname)

	# 2. Find flash, set limits, and remove flash frames
	if imarray.shape[1]>200:
		imarray = imarray[:,0:200,:,:,:,:];

	imarray2 = adjust_sequence(imarray,type)

	# 3. Apply mask
	print('Masking:');print(str(datetime.now()))
	imarray2 = masking(imarray2);

	return imarray2, res, time;

def masking(imarray):
	#Generate Mask
	#Input: imarray - 6D where 0,t,0,z,y,x
	#Output: masked imarray - where 0,t,0,z,y,x

	dmip = np.mean(imarray[:,0:4,:,:,:,:].astype('uint64'),axis=1)
	mip=np.max(imarray.astype('uint64'),axis=1)-dmip;
	#mip=np.divide(np.abs(mip),np.max(imarray2.astype('int16'),axis=1))*1000;

	#diffmaxip, diffminip, diffmeanip = diff_projection(imarray2[0,:,0,:,:,:],0,imarray2.shape[1]);
	mip = np.squeeze(mip);
	# mip = gaussian(mip, sigma=0.8)
	mipb = np.exp(mip) < 1e+17;
	mipb = closing(mipb, ball(4));
	mipb = opening(mipb, ball(8));
	mipb = erosion(mipb, ball(14));
	#mipb = erosion(mipb, ball(8));
	mip[mipb] = 0;mip[~mipb] = 1;
	#sitk.WriteImage(sitk.GetImageFromArray(mip), ('MASK.nii'));

	imarray = np.ma.array(imarray, mask=imarray*mipb[np.newaxis,np.newaxis,np.newaxis,:,:,:]) 
	# plt.imshow(imarray[0,20,0,80,:,:]);
	# plt.show();
	return imarray

def adjust_sequence(imarray,type):
	# looks for the first position that is x% (in decimal) above the average of the first three frames -1
	x=0;
	t0 = imarray[:,0,:,:,:,:].mean()
	t1 = imarray[:,1,:,:,:,:].mean()
	t2 = imarray[:,2,:,:,:,:].mean()
	tavg = np.mean([t0,t1,t2]);
	shapes = imarray.shape;
	for i in xrange(3,shapes[1]):
		tn = imarray[0,i,0,:,:,:].mean()
		if tn >= tavg*(x+1):
			start = i-3;
			break;
	return imarray[:,start:shapes[1],:,:,:,:];

def diff_projection(imgVec,start_frame,end_frame):
	# imageVec is the input image volume with dimensions time,z,y,x
	img_diff=[]
	for i in range(start_frame,end_frame):
	 	img_diff.append(np.absolute(np.squeeze(imgVec[i,:,:,:])-np.squeeze(imgVec[i-1,:,:,:])))    
	img_diff_Vec=np.asarray(img_diff)
	mip_max=np.max(img_diff_Vec,0)
	mip_min=np.min(img_diff_Vec,0)
	mip_mean=np.mean(img_diff_Vec,0) 
	return mip_max, mip_min, mip_mean

def paramap(img, res, res2, time):
	# Code does:
	#1. Reads masked 4D image segment, TIC fitting function name, number of paramters, c. image and windowing info
	#2. Downsample image and find bounding box
	#3. Creates X number of blank images to be filed based on number of paramters
	#4. Generates windows. 
	#5. Builds X parametric maps based on number of paramters

	#1a. Windowing and image info
	windSize = (2,2,2);
	stepSize = (1,1,1);
	voxelscale_org = res[0]*res[1]*res[2];
	voxelscale_res = res2[0]*res2[1]*res2[2];
	#voxelscale_res = voxelscale_org;
	compression = 24.09; #42.98 

	#1b. Creat time points
	timeconst = time/(img.shape[1]+1);
	times = np.arange(1,img.shape[1]+1);

	#2. Downsample image and find bounding box
	print('Downsampling:');print(str(datetime.now()))
	orgimg = img;
	img = resampler(orgimg, orgimg.shape, 0, res, res2);

	#3. Create blank version of array - for final image(s). Based on an input number for x paramteres based on function used.
	# Allows for maximum 5 parametric maps per type of imaging or model.
	maps = np.zeros((5,1,1,img.shape[3],img.shape[4],img.shape[5]));
	maps2 = np.zeros((5,1,1,orgimg.shape[3],orgimg.shape[4],orgimg.shape[5]));

	#4. Make my windows
	windows = sliding_window(img,(1,1,1,windSize[2],windSize[1],windSize[0]),(1,1,1,stepSize[2],stepSize[1],stepSize[0]), False);

	#5. Build array of windowed values >>> Ultra nested loop.
	print('Paraloop start:');print(str(datetime.now()))
	i=0;
	for x in xrange(int(floor(windSize[0]/2)),int(img.shape[5]-floor(windSize[0]/2)),stepSize[0]):
		j=0;
		for y in xrange(int(floor(windSize[1]/2)),int(img.shape[4]-floor(windSize[1]/2)),stepSize[1]):
		  	k=0;
		  	for z in xrange(int(floor(windSize[2]/2)),int(img.shape[3]-floor(windSize[2]/2)),stepSize[2]):
			    TIC = []; TICtime = [];TICz=[]; ddd = [];params = [];popt=[];RMSE = [];#Restart the TIC each time
			    #Get the TIC for the specific window-voxel - This is where I could really speed things up using Numpy.
			    for t in xrange(0,img.shape[1]):
			    	TICtime.append(times[t]); tmpwin=[];
			    	tmpwin = windows[0,t,0,k,j,i,0,0,0,:,:,:];
			    	TIC.append(np.exp(tmpwin[~np.isnan(tmpwin)].mean()/compression)/voxelscale_res);
			    
			    # Iterate k and filter TIC ready
			    k=k+1;
			    TICz = np.array([TICtime,TIC]).astype('float64'); TICz = TICz.transpose();
			    TICz[:,1]=TICz[:,1]-np.mean(TICz[0:4,1]);#Substract noise in TIC before contrast.
			    if TICz[TICz<0].any():#make the smallest number in the TIC 0.
			    	TICz[:,1]=TICz[:,1]+np.min(TICz[:,1]);
			    else:
			    	TICz[:,1]=TICz[:,1]-np.min(TICz[:,1]);

			    #Normalize array and do a bunch of checks
			    normalizer = np.max(TICz[:,1]);
			    TICz[:,1] = TICz[:,1]/normalizer;
			    if normalizer == 0:
			    	#print('Skip: max of 0')
			    	continue;
			    if np.isnan(np.sum(TICz[:,1])):
			    	#print('Skip: nan')
			    	continue;
			    if np.isinf(np.sum(TICz[:,1])):
			    	#print('Skip: inf');
			    	continue;

			    #plt.plot(TICz[:,0],TICz[:,1]); plt.show();
			    #This avoids runtime errors (try and except)
			    try:
			    	params, popt, RMSE = data_fit(TICz,'bolus_lognormal',normalizer,timeconst);#print('before fix:');print(params);print(Rs);
			    	#print('Trying fit')
			    except RuntimeError:
					#print("Error - curve_fit failed");
					continue;
					
			    #Generate masks for blending - blending is (old + new)/2 if there is an old, else it's just new
			    if RMSE > 3:
			    	#print('Skip: RMSE > 3')
			    	continue; #params = np.zeros(params.shape);#print('after fix:');print(params);#Plot check of TIC and fit
			    
			    # #CONTINIOUS PLOTING FOR VERFICATIONS
			    # yaj = bolus_lognormal(TICz[:,0],popt[0],popt[1],popt[2],popt[3]);
			    # plt.plot(TICz[:,0],TICz[:,1],'x',TICz[:,0],yaj[:],'r-');
			    # plt.show();

			    #Generate final para maps
			    for p in xrange(params.shape[0]):
			    	ddd1 = [];ddd2=[];
			    	ddd1 = maps[p,0,0,(z-int(floor(windSize[2]/2))):(z+int(floor(windSize[2]/2))),(y-int(floor(windSize[1]/2))):(y+int(floor(windSize[1]/2))),(x-int(floor(windSize[0]/2))):(x+int(floor(windSize[0]/2)))]
			    	ddd2 = np.where(ddd1 > 0,2,1); # masks resulting array (dd2) with 1 where it is 0 and 2 where more -- Blending within the windows instead of space between step sizes. Blending takes average of the two values set to be at the same location > Could be implemented better - maybe take median?
			    	maps[p,0,0,(z-int(floor(windSize[2]/2))):(z+int(floor(windSize[2]/2))),(y-int(floor(windSize[1]/2))):(y+int(floor(windSize[1]/2))),(x-int(floor(windSize[0]/2))):(x+int(floor(windSize[0]/2)))] = (maps[p,0,0,(z-int(floor(windSize[2]/2))):(z+int(floor(windSize[2]/2))),(y-int(floor(windSize[1]/2))):(y+int(floor(windSize[1]/2))),(x-int(floor(windSize[0]/2))):(x+int(floor(windSize[0]/2)))]+params[p])/ddd2;
		  	j=j+1;
		i=i+1;
	#plt.show();
	#For resampling - uncomment this and use maps 2:
	print('Upsampling:');print(str(datetime.now()))
	tmp = np.zeros((1,maps.shape[1],1,maps.shape[3],maps.shape[4],maps.shape[5]));
	orgsizes2 = (1,1,1,orgimg.shape[3],orgimg.shape[4],orgimg.shape[5]);print(orgsizes2)
	for p in xrange(maps.shape[0]):
		tmp[0,0,0,:,:,:] = maps[p,0,0,:,:,:]
		maps2[p,0,0,:,:,:] = resampler(tmp, tmp.shape, orgimg.shape, res2, res);
	#Sending out resampled map with new res.
	return res, maps2;

def resampler(imarray, curshape, newshape, curres, newres):
	if newshape == 0:
		axial = curshape[4]*curres[1];#y
		lateral = curshape[5]*curres[0];#x
		width = curshape[3]*curres[2];#z
		#return block_reduce(imarray, block_size=(1,1,1,newres/res[2],newres/res[1],newres/res[0]), fund=np.mean)
		imarray2 = np.zeros((1,curshape[1],1,int(width/newres[2]),int(axial/newres[1]),int(lateral/newres[0])));
		for t in xrange(curshape[1]):
			imarray2[0,t,0,:,:,:] = skimage.transform.resize(imarray[0,t,0,:,:,:], (int(width/newres[2]),int(axial/newres[1]),int(lateral/newres[0])));
			#imarray2[0,t,0,:,:,:] = scipy.misc.imresize(imarray[0,t,0,:,:,:], [int(width/newres[2]),int(axial/newres[1]),int(lateral/newres[0])], interp='bilinear');
	else:
		imarray2 = np.zeros((1,curshape[1],1,newshape[3],newshape[4],newshape[5]));
		for t in xrange(curshape[1]):
			imarray2[0,t,0,:,:,:] = skimage.transform.resize(imarray[0,t,0,:,:,:], (newshape[3],newshape[4],newshape[5]));
			#imarray2[0,t,0,:,:,:] = scipy.misc.imresize(imarray[0,t,0,:,:,:], [newshape[3],newshape[4],newshape[5]], interp='bilinear');
	return imarray2;

def data_fit(TIC,model,normalizer,timeconst):
	#Fitting function for bolus to lognormal
	#Returns the parameters scaled by normalizer
	#Beware - all fitting - minimization is done with data normalized 0 to 1. 
	if model == 'bolus_lognormal':
		#kwargs = {"max_nfev":5000}
		popt, pcov = curve_fit(bolus_lognormal, TIC[:,0], TIC[:,1], p0=(1.0,3.0,0.5,0.1),bounds=([0., 0., 0., -1.], [np.inf, np.inf, np.inf, 10.]),method='trf')#p0=(1.0,3.0,0.5,0.1) ,**kwargs
		auc = popt[0]; rauc=normalizer*popt[0]; mu=popt[1]; sigma=popt[2]; t0=popt[3]; mtt=timeconst*np.exp(mu+sigma*sigma/2);
		tp = timeconst*exp(mu-sigma*sigma);pe=normalizer*bolus_lognormal(exp(mu-sigma*sigma), popt[0], popt[1], popt[2], popt[3]);
		params = np.array([pe, rauc, tp, mtt]);

		# Get error parameters
		residuals = TIC[:,1] - bolus_lognormal(TIC[:,0], popt[0], mu, sigma, t0);
		ss_res = np.sum(residuals[~np.isnan(residuals)]**2);# Residual sum of squares
		ss_tot = np.sum((TIC[:,1]-np.mean(TIC[:,1]))**2);# Total sum of squares
		r_squared = 1 - (ss_res / ss_tot);# R squared
		RMSE = (scipy.sum(residuals[~np.isnan(residuals)]**2)/(residuals[~np.isnan(residuals)].size-2))**0.5;# RMSE
		#MSE = mean_squared_error(TIC[:,1], bolus_lognormal(TIC[:,0], popt[0], mu, sigma, t0))

		return params, popt, RMSE;

def bolus_lognormal(x, auc, mu, sigma, t0):        
    curve_fit=(auc/(2.5066*sigma*(x-t0)))*np.exp(-1*(((np.log(x-t0)-mu)**2)/(2*sigma*sigma))) 
    return np.nan_to_num(curve_fit)
    
def bolus_lagmodel(x, auc, landa, mu, sigma):
    curve_fit=(auc/2)*landa*np.exp(-landa*x+landa*mu+0.5*(landa**2)*(sigma**2))*(1+sp.special.erf( (x-mu-landa*(sigma**2))/(np.sqrt(2*(sigma**2))) ))  
    return np.nan_to_num(curve_fit) 
    
def bolus_gammamodel(x, auc, beta, alpha, t0):
    alpha1=alpha+1
    curve_fit=(auc/((beta** alpha1)*sp.special.gamma(alpha1)))*((x-t0)**(alpha1-1))*np.exp(-(x-t0)/beta)
    return np.nan_to_num(curve_fit)   
    
def bolus_FPTmodel(x, auc, landa, mu, t0):
    curve_fit=auc*(np.exp(landa)/mu)* 0.3989*np.sqrt(landa)*((mu/(x-t0))**1.5)*np.exp(-0.5*landa*((mu/(x-t0))+((x-t0)/mu)))  
    return np.nan_to_num(curve_fit)
    
def bolus_LDRWmodel(x, auc, landa, mu, t0):
    curve_fit=auc*((np.exp(landa))/mu)*np.sqrt((mu/(x-t0))*(landa/6.2832))*np.exp(-0.5*landa*((mu/(x-t0))+((x-t0)/mu)))    
    return np.nan_to_num(curve_fit)

def sliding_window(a,ws,ss = None,flatten = True):
	# '''
	# Return a sliding window over a in any number of dimensions

	# Parameters:
	#     a  - an n-dimensional numpy array
	#     ws - an int (a is 1D) or tuple (a is 2D or greater) representing the size 
	#          of each dimension of the window
	#     ss - an int (a is 1D) or tuple (a is 2D or greater) representing the 
	#          amount to slide the window in each dimension. If not specified, it
	#          defaults to ws.
	#     flatten - if True, all slices are flattened, otherwise, there is an 
	#               extra dimension for each dimension of the input.

	# Returns
	#     an array containing each n-dimensional window from a
	# '''
	if None is ss:
	  	# ss was not provided. the windows will not overlap in any direction.
		ss = ws
	ws = norm_shape(ws)
	ss = norm_shape(ss)

	# convert ws, ss, and a.shape to numpy arrays so that we can do math in every 
	# dimension at once.
	ws = np.array(ws)
	ss = np.array(ss)
	shape = np.array(a.shape)


	# ensure that ws, ss, and a.shape all have the same number of dimensions
	ls = [len(shape),len(ws),len(ss)]
	if 1 != len(set(ls)):
	  raise ValueError(\
	  'a.shape, ws and ss must all have the same length. They were %s' % str(ls))

	# ensure that ws is smaller than a in every dimension
	if np.any(ws > shape):
	  raise ValueError(\
	  'ws cannot be larger than a in any dimension a.shape was %s and ws was %s' % (str(a.shape),str(ws)))

	# how many slices will there be in each dimension?
	newshape = norm_shape(((shape - ws) // ss) + 1)
	# the shape of the strided array will be the number of slices in each dimension
	# plus the shape of the window (tuple addition)
	newshape += norm_shape(ws)
	# the strides tuple will be the array's strides multiplied by step size, plus
	# the array's strides (tuple addition)
	newstrides = norm_shape(np.array(a.strides) * ss) + a.strides
	strided = ast(a,shape = newshape,strides = newstrides)
	if not flatten:
	  return strided

	# Collapse strided so that it has one more dimension than the window.  I.e.,
	# the new array is a flat list of slices.
	meat = len(ws) if ws.shape else 0
	firstdim = (np.product(newshape[:-meat]),) if ws.shape else ()
	dim = firstdim + (newshape[-meat:])
	# remove any dimensions with size 1
	dim = filter(lambda i : i != 1,dim)

	return strided.reshape(dim);

def norm_shape(shape):
	# '''
	# Normalize numpy array shapes so they're always expressed as a tuple, 
	# even for one-dimensional shapes.

	# Parameters
	#     shape - an int, or a tuple of ints

	# Returns
	#     a shape tuple
	# '''
	try:
	  i = int(shape)
	  return (i,)
	except TypeError:
	  # shape was not a number
	  pass

	try:
	  t = tuple(shape)
	  return t
	except TypeError:
	  # shape was not iterable
	  pass

	raise TypeError('shape must be an int, or a tuple of ints')

def view4d(imarray2,p,t,c,z,y,x):
	# Display and scroll through stack of 4D along t and z.
	cv2.namedWindow("Original1", cv2.WINDOW_NORMAL)
	imarray3 = cv2.resize(imarray2[p,0,0,0,:,:],(y*1,x*1))
	cv2.imshow("Original1", imarray3)

	# plt.imshow(imarray2[1,1,:,:])
	# plt.show()

	thresholdlevelT = 0
	thresholdlevelZ = 0

	while True:
		# Need an if statement for min max of z and t.
		k = cv2.waitKey(0) & 0xff
		if k == 27: # ESC
			cv2.destroyAllWindows()
			break
		elif k == 1: # downkey
			thresholdlevelZ = (thresholdlevelZ - 1)
			imarray3 = cv2.resize(imarray2[p,thresholdlevelT,0,thresholdlevelZ,:,:],(y*1,x*1))
			cv2.imshow("Original1", imarray3)
			# plt.imshow(imarray3[thresholdlevelT,thresholdlevelZ,:,:])
			# plt.show()
		elif k == 0: # upkey
			thresholdlevelZ = (thresholdlevelZ + 1)
			imarray3 = cv2.resize(imarray2[p,thresholdlevelT,0,thresholdlevelZ,:,:],(y*1,x*1))
			cv2.imshow("Original1", imarray3)
			# plt.imshow(imarray3[thresholdlevelT,thresholdlevelZ,:,:])
			# plt.show()
		elif k == 2: # leftkey
			thresholdlevelT = (thresholdlevelT - 1)
			imarray3 = cv2.resize(imarray2[p,thresholdlevelT,0,thresholdlevelZ,:,:],(y*1,x*1))
			cv2.imshow("Original1", imarray3)
			# plt.imshow(imarray3[thresholdlevelT,thresholdlevelZ,:,:])
			# plt.show()
		elif k == 3: # rightkey	
			thresholdlevelT = (thresholdlevelT + 1)
			imarray3 = cv2.resize(imarray2[p,thresholdlevelT,0,thresholdlevelZ,:,:],(y*1,x*1))
			cv2.imshow("Original1", imarray3)
			# plt.imshow(imarray3[thresholdlevelT,thresholdlevelZ,:,:])
			# plt.show()

def read_xmlraw_image_func(filename):    
	# get .raw filename
	filename_raw=filename[0:len(filename)-3]+('0.raw');
	fff = open(filename_raw,'rb')

	# parsing xml file
	tree = ET.parse(filename);
	root = tree.getroot();
	for i in range(0, len(root)):
	   if  root[i].tag=='Columns':
	      M=int(root[i].text);
	   if  root[i].tag=='Rows':
	      N=int(root[i].text);  
	   if  (root[i].find('Geometry') is None) is False:
	      P=int(root[i].find('Geometry').find('Layers').find('Layer').find('RegionLocationMaxz1').text)+1;
	      voxelX=float(root[i].find('Geometry').find('Layers').find('Layer').find('PhysicalDeltaX').text);
	      voxelY=float(root[i].find('Geometry').find('Layers').find('Layer').find('PhysicalDeltaY').text);
	      voxelZ=float(root[i].find('Geometry').find('Layers').find('Layer').find('PhysicalDeltaZ').text);
	      voxel=[voxelX*10, voxelY*10, voxelZ*10]; 
	   if  root[i].tag=='AcquisitionDateTime':
	      tval=root[i].text;
	      dateStr=tval[0:4]+'-'+tval[4:6]+'-'+tval[6:8]+' '+tval[8:10]+':'+tval[10:12]+':'+tval[12:len(tval)];
	      time=float(tval[12:len(tval)])+float(tval[10:12])*60+float(tval[8:10])*3600;
	#print(M,N,P,voxel,tval,dateStr,time);
	shapes = (M,N,P);
	x = np.fromfile(fff,dtype=np.uint8)
	img = np.reshape(x, (P,N,M))

	return img, voxel, time, shapes, dateStr