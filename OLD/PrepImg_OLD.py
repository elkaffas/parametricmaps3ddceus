from __future__ import print_function
import cv2
import PIL
import numpy as np
import scipy as sp 
import scipy.misc
import matplotlib
#import matplotlib.pyplot as plt
#import matplotlib.pyplot as plt
#import matplotlib
#import matplotlib.pyplot as plt
#import matplotlib.pyplot as plt
#from matplotlib import pyplot as plt
from matplotlib import pyplot as plt
from PIL import Image
import tifffile as tff
import libtiff
import dicom 
# import vtk

#Code Does: 
#1 read in a 4D image or a set of 3D images
#2 Find flashes if any
#3 Scale down
#4 Apply mask and bounding Box
#5 Pass a blank version of the original size image (with zeros) and the new image. 


# Loading 4D
info = dicom.read_file('Test3DCEUSData/4D/m939conBD104D.dcm')
t = info.NumberOfTemporalPositions
im = tff.TiffFile('Test3DCEUSData/4D/m939conBD104D.tif')
imarray = im.asarray()
x = imarray.shape[2]
y = imarray.shape[1]
z = imarray.shape[0]/t
imarray2 = np.reshape(imarray, (t,z,y,x))
print(imarray2.shape)

# Display and scroll through stack of 4D along t and z.
cv2.namedWindow("Original1", cv2.WINDOW_NORMAL)
imarray3 = cv2.resize(imarray2[1,1,:,:],(y*3,x*3))
cv2.imshow("Original1", imarray3)

# plt.imshow(imarray2[1,1,:,:])
# plt.show()

thresholdlevelT = 1
thresholdlevelZ = 1

while True:
	# Need an if statement for min max of z and t.
	k = cv2.waitKey(0) & 0xff
	if k == 27: # ESC
		cv2.destroyAllWindows()
		break
	elif k == 1: # downkey
		thresholdlevelZ = (thresholdlevelZ - 1)
		imarray3 = cv2.resize(imarray2[thresholdlevelT,thresholdlevelZ,:,:],(y*3,x*3))
		cv2.imshow("Original1", imarray3)
		# plt.imshow(imarray3[thresholdlevelT,thresholdlevelZ,:,:])
		# plt.show()
	elif k == 0: # upkey
		thresholdlevelZ = (thresholdlevelZ + 1)
		imarray3 = cv2.resize(imarray2[thresholdlevelT,thresholdlevelZ,:,:],(y*3,x*3))
		cv2.imshow("Original1", imarray3)
		# plt.imshow(imarray3[thresholdlevelT,thresholdlevelZ,:,:])
		# plt.show()
	elif k == 2: # leftkey
		thresholdlevelT = (thresholdlevelT - 1)
		imarray3 = cv2.resize(imarray2[thresholdlevelT,thresholdlevelZ,:,:],(y*3,x*3))
		cv2.imshow("Original1", imarray3)
		# plt.imshow(imarray3[thresholdlevelT,thresholdlevelZ,:,:])
		# plt.show()
	elif k == 3: # rightkey	
		thresholdlevelT = (thresholdlevelT + 1)
		imarray3 = cv2.resize(imarray2[thresholdlevelT,thresholdlevelZ,:,:],(y*3,x*3))
		cv2.imshow("Original1", imarray3)
		# plt.imshow(imarray3[thresholdlevelT,thresholdlevelZ,:,:])
		# plt.show()











