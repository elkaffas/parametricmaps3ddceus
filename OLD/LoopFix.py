	
def generate_TIC(times,window,compression,voxelscale):
	TIC = []; TICtime = [];TICz=[]; 
	for t in xrange(0,times.shape[0]):
		TICtime.append(times[t]); tmpwin=[];
		tmpwin = window[t,:,:,:];
		TIC.append(np.exp(tmpwin[~np.isnan(tmpwin)].mean()/compression)/voxelscale);
	TICz = np.array([TICtime,TIC]).astype('float64'); TICz = TICz.transpose();
	TICz[:,1]=TICz[:,1]-np.mean(TICz[0:4,1]);#Substract noise in TIC before contrast.
	if TICz[TICz<0].any():#make the smallest number in the TIC 0.
		TICz[:,1]=TICz[:,1]+np.min(TICz[:,1]);
	else:
		TICz[:,1]=TICz[:,1]-np.min(TICz[:,1]);
	return TICz;

def generate_maps(params,maps,windSize,z,y,x):
	#Generate final para maps
	for p in xrange(params.shape[0]):
		ddd1 = [];ddd2=[];
		ddd1 = maps[p,0,0,(z-int(floor(windSize[2]/2))):(z+int(floor(windSize[2]/2))),(y-int(floor(windSize[1]/2))):(y+int(floor(windSize[1]/2))),(x-int(floor(windSize[0]/2))):(x+int(floor(windSize[0]/2)))]
		ddd2 = np.where(ddd1 > 0,2,1); # masks resulting array (dd2) with 1 where it is 0 and 2 where more -- Blending within the windows instead of space between step sizes. Blending takes average of the two values set to be at the same location > Could be implemented better - maybe take median?
		maps[p,0,0,(z-int(floor(windSize[2]/2))):(z+int(floor(windSize[2]/2))),(y-int(floor(windSize[1]/2))):(y+int(floor(windSize[1]/2))),(x-int(floor(windSize[0]/2))):(x+int(floor(windSize[0]/2)))] = (maps[p,0,0,(z-int(floor(windSize[2]/2))):(z+int(floor(windSize[2]/2))),(y-int(floor(windSize[1]/2))):(y+int(floor(windSize[1]/2))),(x-int(floor(windSize[0]/2))):(x+int(floor(windSize[0]/2)))]+params[p])/ddd2;
	return maps;

def paramap_core(times,xlist,ylist,zlist,windows,compression,voxelscale_res,timeconst,maps,windSize):
	#Function to include all the cores of paramap

####################################################################################################
	xlist = np.arange(int(floor(windSize[0]/2)),int(img.shape[5]-floor(windSize[0]/2)),stepSize[0]);
	ylist = np.arange(int(floor(windSize[1]/2)),int(img.shape[4]-floor(windSize[1]/2)),stepSize[1]);
	zlist = np.arange(int(floor(windSize[2]/2)),int(img.shape[3]-floor(windSize[2]/2)),stepSize[2]);

	for index in np.ndindex(xlist.shape[0], ylist.shape[0], zlist.shape[0]):
		indexinv = index[::-1]; TICz=[];params = [];popt=[];
		x = xlist[index[0]];y = ylist[index[1]];z = zlist[index[2]]; 
		k = index[2];j=index[1];i=index[0];
		TICz = generateTIC(times,windows[0,:,0,k,j,i,0,0,0,:,:,:],compression,voxelscale_res)
		
		#Normalize array and do a bunch of checks/filters on TIC
	    normalizer = np.max(TICz[:,1]);
	    TICz[:,1] = TICz[:,1]/normalizer;
	    if normalizer == 0:
	    	#print('Skip: max of 0')
	    	continue;
	    if np.isnan(np.sum(TICz[:,1])):
	    	#print('Skip: nan')
	    	continue;
	    if np.isinf(np.sum(TICz[:,1])):
	    	#print('Skip: inf');
	    	continue;

	    # Do the fitting
	    try:
	    	params, popt, RMSE = data_fit(TICz,'bolus_lognormal',normalizer,timeconst);#print('before fix:');print(params);print(Rs);
	    	#print('Trying fit')
	    except RuntimeError:
			#print("Error - curve_fit failed");
			continue;
			
	    #Generate masks for blending - blending is (old + new)/2 if there is an old, else it's just new
	    if RMSE > 3:
	    	#print('Skip: RMSE > 3')
	    	continue; #params = np.zeros(params.shape);#print('after fix:');print(params);#Plot check of TIC and fit

	    #Generate blended para maps
	    maps = generate_maps(params,maps,windSize,z,y,x):

####################################################################################################
	i=0;
	for x in xrange(int(floor(windSize[0]/2)),int(img.shape[5]-floor(windSize[0]/2)),stepSize[0]):
		j=0;
		for y in xrange(int(floor(windSize[1]/2)),int(img.shape[4]-floor(windSize[1]/2)),stepSize[1]):
		  	k=0;
		  	for z in xrange(int(floor(windSize[2]/2)),int(img.shape[3]-floor(windSize[2]/2)),stepSize[2]):
			    TICz=[];params = [];popt=[];
			    TICz = generateTIC(times,windows[0,t,0,k,j,i,0,0,0,:,:,:],compression,voxelscale_res)

			    # Iterate k
			    k=k+1;

			    #Normalize array and do a bunch of checks/filters on TIC
			    normalizer = np.max(TICz[:,1]);
			    TICz[:,1] = TICz[:,1]/normalizer;
			    if normalizer == 0:
			    	#print('Skip: max of 0')
			    	continue;
			    if np.isnan(np.sum(TICz[:,1])):
			    	#print('Skip: nan')
			    	continue;
			    if np.isinf(np.sum(TICz[:,1])):
			    	#print('Skip: inf');
			    	continue;

			    # Do the fitting
			    try:
			    	params, popt, RMSE = data_fit(TICz,'bolus_lognormal',normalizer,timeconst);#print('before fix:');print(params);print(Rs);
			    	#print('Trying fit')
			    except RuntimeError:
					#print("Error - curve_fit failed");
					continue;
					
			    #Generate masks for blending - blending is (old + new)/2 if there is an old, else it's just new
			    if RMSE > 3:
			    	#print('Skip: RMSE > 3')
			    	continue; #params = np.zeros(params.shape);#print('after fix:');print(params);#Plot check of TIC and fit

			    #Generate para maps
			    maps = generate_maps(params,maps,windSize,z,y,x):
			j=j+1;
		i=i+1;