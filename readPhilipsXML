from __future__ import print_function
import sys, os, glob
import numpy as np
from numpy.lib.stride_tricks import as_strided as ast
import scipy as sp 
import scipy.misc
from scipy import stats
from scipy.optimize import curve_fit
import scipy.ndimage as nd
from math import exp, floor, ceil
from time import ctime, sleep
from datetime import datetime
import xml.etree.ElementTree as ET
import dicom
from sklearn.metrics import mean_squared_error
import skimage.transform
from skimage.morphology import opening, disk,closing,ball,erosion, dilation
from skimage.filters import gaussian, threshold_otsu, sobel, rank
from skimage.measure import label, regionprops
from sklearn.metrics import mean_squared_error
import nibabel as nib
from joblib import Parallel, delayed
import multiprocessing
import tempfile
import shutil

#### Should not be used on Sherlock
#from matplotlib import pyplot as plt
#from matplotlib.pyplot import figure, show
#from lmfit import  Model 
#import cv2
#import PIL
#from PIL import Image
#from resizeimage import resizeimage
#import tifffile as tff
#import libtiff
#from pylab import *
#from medpy.io import load, save
#import SimpleITK as sitk
#from skimage.viewer import ImageViewer
#####

#Run Instructions With: 
# import readPhilipsXML as rPX
# imarray, res, time = rPX.read3D(data,cut=cut)

def read_xmlraw_image_func(filename):    
	# get .raw filename
	filename_raw=filename[0:len(filename)-3]+('0.raw');
	fff = open(filename_raw,'rb')

	# parsing xml file
	tree = ET.parse(filename);
	root = tree.getroot();

	# ADD MAX FRAMES TO LOAD
	numfiles = len(root); # Comment this out if using max num frames on next lines
	# if len(root)>250:
	# 	numfiles = 250;
	# else:
	# 	numfiles = len(root);

	for i in range(0, numfiles):
		if  root[i].tag=='Columns':
		  M=int(root[i].text);
		if  root[i].tag=='Rows':
		  N=int(root[i].text);  
		if  (root[i].find('Geometry') is None) is False:
		  P=int(root[i].find('Geometry').find('Layers').find('Layer').find('RegionLocationMaxz1').text)+1;
		  voxelX=float(root[i].find('Geometry').find('Layers').find('Layer').find('PhysicalDeltaX').text);
		  voxelY=float(root[i].find('Geometry').find('Layers').find('Layer').find('PhysicalDeltaY').text);
		  voxelZ=float(root[i].find('Geometry').find('Layers').find('Layer').find('PhysicalDeltaZ').text);
		  voxel=[voxelX*10, voxelY*10, voxelZ*10]; 
		if  root[i].tag=='AcquisitionDateTime':
		  tval=root[i].text;
		  dateStr=tval[0:4]+'-'+tval[4:6]+'-'+tval[6:8]+' '+tval[8:10]+':'+tval[10:12]+':'+tval[12:len(tval)];
		  time=float(tval[12:len(tval)])+float(tval[10:12])*60+float(tval[8:10])*3600;

	#print(M,N,P,voxel,tval,dateStr,time);
	shapes = (M,N,P);
	x = np.fromfile(fff,dtype=np.uint8)
	img = np.reshape(x, (P,N,M))

	return img, voxel, time, shapes, dateStr

def read3D(data, newres=0,cut=[[-1,-1,5,5],[-1,-1,25,5],[-1,-1,5,5]]):
	# cut=[[10,15,5,5],[50,5,20,4],[15,15,5,5]] #Size reduce with user selected caps
	# cut=[[-1,-1,5,5],[-1,-1,5,5],[-1,-1,5,5]] #Automatic size reduce
	# cut=[[0,0,5,5],[0,0,20,4],[0,0,5,5]] # Keep original size      
	#print(cut,newres);
	N_lines_z_axis_cut=cut[0][0:2] #10,10	
	N_lines_z_axis_cut_limit=cut[0][2:4]#5,5
	N_lines_y_axis_cut=cut[1][0:2]#50,5	
	N_lines_y_axis_cut_limit=cut[1][2:4]#20,5
	N_lines_x_axis_cut=cut[2][0:2]##[15, 15]	
	N_lines_x_axis_cut_limit=cut[2][2:4]#5,5
	xmldir = data+('/*.xml');
	xmlnamedir = sorted(glob.glob(xmldir));
	
	# ADD MAX FRAMES TO LOAD
	# if len(xmlnamedir)>250:
	# 	xmlnamedir = xmlnamedir[0:250];
	    
	#img, res, timeinitial, shapes, dateStr = read_xmlraw_image_func(xmlnamedir[0])
	#imarray = np.zeros((1,len(xmlnamedir),1,shapes[2],shapes[1],shapes[0]),dtype='uint8')
	imarray = []#np.zeros((len(xmlnamedir),shapes[2],shapes[1],shapes[0]),dtype='uint8')
	timeinitial = -1000; ix=-1;
	imi_mid, res, timelast, shapes, dateStr = read_xmlraw_image_func(xmlnamedir[np.uint16(len(xmlnamedir)/2)]); 
	imi10, res, timelast, shapes, dateStr = read_xmlraw_image_func(xmlnamedir[10]); 
	imi_mid, res, timelast, shapes, dateStr = read_xmlraw_image_func(xmlnamedir[1]); 

	for xmlname in xmlnamedir:
		#imarray[0,xmlnamedir.index(xmlname),0,:,:,:], res, timelast, shapes, dateStr = read_xmlraw_image_func(xmlname)
		imi, res, timelast, shapes, dateStr = read_xmlraw_image_func(xmlname); 
		sz=imi.shape
		if timeinitial==-1000: 			
			timeinitial = timelast
			mp=np.mean(imi10,0)+np.mean(imi_mid,0);
			mpb=mp>0.3*threshold_otsu(mp); 
			lc=rank.otsu(mp.astype('uint16'), disk(10)); mpb2=mp>0.7*np.mean(lc); 
			mpb=(mpb+mpb2)>0;
			mp1=np.sum(mpb[:,40:mp.shape[1]-20],1);
			ix = np.where(mp1>0)
			mp0=np.sum(mpb,0)
			ix0 = np.where(mp0>0)          
			mpz=np.mean(imi10,1)+np.mean(imi_mid,1)+np.mean(imi,1);
			mpzb=mpz>1.2*threshold_otsu(mpz)           
			mpz1=np.sum(mpzb,1)
			iz = np.where(mpz1>0)     
			#ix[0][-1]=sz[1];ix[0][0]=0; #for test
			#ix0[0][-1]=sz[2];ix0[0][0]=0; #for test          
			#iz[0][-1]=sz[0];iz[0][0]=0; #for test   

			if N_lines_y_axis_cut[0]!=0: 
				N_lines_y_axis_cut[0]=max(0,max(N_lines_y_axis_cut[0]*(ix[0][0]>0),ix[0][0]-N_lines_y_axis_cut_limit[0])); 
			if N_lines_y_axis_cut[1]!=0:          
			   	N_lines_y_axis_cut[1]=sz[1]-min(sz[1],max((sz[1]-N_lines_y_axis_cut[1])*(ix[0][-1]<sz[1]),ix[0][-1]+N_lines_y_axis_cut_limit[1]))

			if N_lines_x_axis_cut[0]!=0:          
			   	N_lines_x_axis_cut[0]=max(0,max(N_lines_x_axis_cut[0]*(ix0[0][0]>0),ix0[0][0]-N_lines_x_axis_cut_limit[0])) 
			if N_lines_x_axis_cut[1]!=0:          
			   	N_lines_x_axis_cut[1]=sz[2]-min(sz[2],max((sz[2]-N_lines_x_axis_cut[1])*(ix0[0][-1]<sz[2]),ix0[0][-1]+N_lines_x_axis_cut_limit[1])) 

			if N_lines_z_axis_cut[0]!=0: 
			   	N_lines_z_axis_cut[0]=max(0,max(N_lines_z_axis_cut[0]*(iz[0][0]>0),iz[0][0]-N_lines_z_axis_cut_limit[0])) 
			if N_lines_z_axis_cut[1]!=0:          
			   	N_lines_z_axis_cut[1]=sz[0]-min(sz[0],max((sz[0]-N_lines_z_axis_cut[1])*(iz[0][-1]<sz[1]),iz[0][-1]+N_lines_z_axis_cut_limit[1]))

			#print(N_lines_z_axis_cut,N_lines_y_axis_cut,N_lines_x_axis_cut)

			print('Orginal image volume size',imi.shape)

	         # reduce matrix size      
		imi=imi[N_lines_z_axis_cut[0]:sz[0]-N_lines_z_axis_cut[1],N_lines_y_axis_cut[0]:sz[1]- N_lines_y_axis_cut[1],N_lines_x_axis_cut[0]:sz[2]-N_lines_x_axis_cut[1]]
		
	         # resampling
		if newres!=0:			   
		   imi=resampler_4d(imi, 0, res, newres)
		imarray.append(imi)

	time = timelast - timeinitial; #total time of cine in seconds.
	if newres!=0:
	          print('voxel size is changed from ', res, 'to voxel size of ', newres)
	print('Rrmeduced image volume size',imi.shape)
	sh1_og=np.shape(imi_mid);sh1_og=(sh1_og[0]*sh1_og[1]*sh1_og[2])/(1024**2)
	sh1=np.shape(imi);sh1=(sh1[0]*sh1[1]*sh1[2])/(1024**2)
	print('Reduction rate: ',np.round(100*sh1/sh1_og),'% of original size-- from ',np.round(sh1_og), ' Mbytes to ', np.round(sh1), ' Mbytes')  
	imarray1 = np.zeros((1,len(xmlnamedir),1,imi.shape[0],imi.shape[1],imi.shape[2]),dtype='uint8')
	imarray1[0,:,0,:,:,:] = np.asarray(imarray)
	imarray=imarray1

	return imarray, res, time;